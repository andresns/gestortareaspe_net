﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Contacto
    {
        private int idContacto;
        private string nombreContacto;
        private string telefonoContacto;

        public int IdContacto { get => idContacto; set => idContacto = value; }
        public string NombreContacto { get => nombreContacto; set => nombreContacto = value; }
        public string TelefonoContacto { get => telefonoContacto; set => telefonoContacto = value; }
    }
}

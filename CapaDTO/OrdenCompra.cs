﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class OrdenCompra
    {
        private string codigoOC;
        private int pedido;
        private int solicitud;
        private string mail_envio;
        private bool facturado;
        private bool enviado;

        public string CodigoOC { get => codigoOC; set => codigoOC = value; }
        public int Pedido { get => pedido; set => pedido = value; }
        public int Solicitud { get => solicitud; set => solicitud = value; }
        public string Mail_envio { get => mail_envio; set => mail_envio = value; }
        public bool Facturado { get => facturado; set => facturado = value; }
        public bool Enviado { get => enviado; set => enviado = value; }
    }
}

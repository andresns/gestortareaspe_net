﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Recordatorio
    {
        private int idRecordatorio;
        private DateTime fechaRecordatorio;
        private bool repetirRecordatorio;
        private bool estadoRecordatorio;
        private bool flagRecordatorio;
        private int idTarea;

        public int IdRecordatorio { get => idRecordatorio; set => idRecordatorio = value; }
        public DateTime FechaRecordatorio { get => fechaRecordatorio; set => fechaRecordatorio = value; }
        public bool RepetirRecordatorio { get => repetirRecordatorio; set => repetirRecordatorio = value; }
        public bool EstadoRecordatorio { get => estadoRecordatorio; set => estadoRecordatorio = value; }
        public bool FlagRecordatorio { get => flagRecordatorio; set => flagRecordatorio = value; }
        public int IdTarea { get => idTarea; set => idTarea = value; }
    }
}

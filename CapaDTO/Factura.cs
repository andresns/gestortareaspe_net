﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Factura
    {
        private int folioFactura;
        private bool despachado;
        private OrdenCompra ordenCompra;

        public int FolioFactura { get => folioFactura; set => folioFactura = value; }
        public bool Despachado { get => despachado; set => despachado = value; }
        public OrdenCompra OrdenCompra { get => ordenCompra; set => ordenCompra = value; }
    }
}

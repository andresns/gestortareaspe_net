﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class Tarea
    {
        private int idTarea;
        private string nombreTarea;
        private string detalleTarea;
        private DateTime fechaCreacionTarea;
        private DateTime fechaModificacionTarea;
        private bool prioridadTarea;
        private bool repetible;
        private DateTime ultimaRepeticion;
        private bool estadoTarea;
        
        public int IdTarea { get => idTarea; set => idTarea = value; }
        public string NombreTarea { get => nombreTarea; set => nombreTarea = value; }
        public string DetalleTarea { get => detalleTarea; set => detalleTarea = value; }
        public DateTime FechaCreacionTarea { get => fechaCreacionTarea; set => fechaCreacionTarea = value; }
        public DateTime FechaModificacionTarea { get => fechaModificacionTarea; set => fechaModificacionTarea = value; }
        public bool PrioridadTarea { get => prioridadTarea; set => prioridadTarea = value; }
        public bool Repetible { get => repetible; set => repetible = value; }
        public DateTime UltimaRepeticion { get => ultimaRepeticion; set => ultimaRepeticion = value; }
        public bool EstadoTarea { get => estadoTarea; set => estadoTarea = value; }
        
    }
}

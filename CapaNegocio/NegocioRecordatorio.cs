﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDTO;
using CapaConexion;
using System.Data;

namespace CapaNegocio
{
    public class NegocioRecordatorio
    {
        private ConexionSQLite conexion;

        public ConexionSQLite Conexion { get => conexion; set => conexion = value; }

        public void configurarConexion()
        {
            this.Conexion = new ConexionSQLite();
            this.Conexion.NombreBaseDatos = "GestorTareasPE_DB";
            this.Conexion.NombreTabla = "recordatorio";
            this.Conexion.CadenaConexion = @"Data Source=..\..\..\CapaConexion\GestorTareasPE_DB.db;";
        } //Fin Configurar Conexion

        public void insertarRecordatorio(Recordatorio recordatorio)
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "INSERT INTO " + this.Conexion.NombreTabla +
                                    " (id_recordatorio, fecha_recordatorio, repetir_recordatorio, estado_recordatorio, fk_id_tarea) " +
                                    "VALUES (null," +
                                            "'" + recordatorio.FechaRecordatorio + "'," +
                                            recordatorio.RepetirRecordatorio + "," +
                                            "0," +
                                            recordatorio.IdTarea + ");";
            this.Conexion.EsSelect = false;
            this.Conexion.conectar();
        } //Fin Insertar Recordatorio

        public void eliminarRecordatorio(int idRecordatorio)
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "DELETE FROM " + this.Conexion.NombreTabla
                                   + " WHERE id_recordatorio = " + idRecordatorio + ";";
            this.Conexion.EsSelect = false;
            this.Conexion.conectar();
        } //Fin Eliminar Recordatorio

        public void modificarRecordatorio(Recordatorio recordatorio)
        {
            this.configurarConexion();

            int intRepetirRecordatorio;
            if (recordatorio.RepetirRecordatorio == false)
            {
                intRepetirRecordatorio = 0;
            }
            else
            {
                intRepetirRecordatorio = 1;
            }

            int intEstadoRecordatorio;
            if (recordatorio.EstadoRecordatorio == false)
            {
                intEstadoRecordatorio = 0;
            }
            else
            {
                intEstadoRecordatorio = 1;
            }

            this.Conexion.CadenaSQL = "UPDATE " + this.Conexion.NombreTabla +
                                   " SET" +
                                   " fecha_recordatorio = '" + recordatorio.FechaRecordatorio + "'," +
                                   " repetir_recordatorio = " + intRepetirRecordatorio + "," +
                                   " estado_recordatorio = " + intEstadoRecordatorio +
                                   " WHERE id_recordatorio = " + recordatorio.IdRecordatorio + ";";
            this.Conexion.EsSelect = false;
            this.Conexion.conectar();
        } //Fin Modificar Recordatorio

        public Recordatorio buscarRecordatorio(int idTarea)
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "SELECT * FROM " + this.Conexion.NombreTabla +
                                      " WHERE fk_id_tarea = " + idTarea + ";";
            this.Conexion.EsSelect = true;
            this.Conexion.conectar();

            Recordatorio auxRecordatorio = new Recordatorio();
            DataTable dt = new DataTable();
            dt = this.Conexion.DbDataSet.Tables[this.Conexion.NombreTabla];

            try
            {
                auxRecordatorio.IdRecordatorio = int.Parse(dt.Rows[0]["id_recordatorio"].ToString());
                auxRecordatorio.FechaRecordatorio = Convert.ToDateTime(dt.Rows[0]["fecha_recordatorio"]);
                auxRecordatorio.RepetirRecordatorio = Convert.ToBoolean(dt.Rows[0]["repetir_recordatorio"]);
                auxRecordatorio.EstadoRecordatorio = Convert.ToBoolean(dt.Rows[0]["estado_recordatorio"]);
                auxRecordatorio.IdTarea = int.Parse(dt.Rows[0]["fk_id_tarea"].ToString());
            }
            catch (Exception ex)
            {
                auxRecordatorio.IdRecordatorio = 0;
            }

            return auxRecordatorio;
        } //Fin Buscar Recordatorio

        public List<Recordatorio> listarRecordatoriosHoy()
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "SELECT * FROM " + this.Conexion.NombreTabla +
                                      " WHERE strftime('%d-%m-%Y',datetime(CURRENT_TIMESTAMP, 'localtime')) = strftime('%d-%m-%Y', datetime(substr(fecha_recordatorio, 7, 4) || '-' || substr(fecha_recordatorio, 4, 2) || '-' || substr(fecha_recordatorio, 1, 2)))" +
                                      " AND estado_recordatorio=0;";
            this.Conexion.EsSelect = true;
            this.Conexion.conectar();

            DataTable dt = new DataTable();
            dt = this.Conexion.DbDataSet.Tables[this.Conexion.NombreTabla];

            List<Recordatorio> auxListaRecordatorios = new List<Recordatorio>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Recordatorio auxRecordatorio = new Recordatorio();
                auxRecordatorio.IdRecordatorio = int.Parse(dt.Rows[i]["id_recordatorio"].ToString());
                auxRecordatorio.FechaRecordatorio = Convert.ToDateTime(dt.Rows[i]["fecha_recordatorio"]);
                auxRecordatorio.RepetirRecordatorio = Convert.ToBoolean(dt.Rows[i]["repetir_recordatorio"]);
                auxRecordatorio.EstadoRecordatorio = Convert.ToBoolean(dt.Rows[i]["estado_recordatorio"]);
                auxRecordatorio.FlagRecordatorio = true;
                auxRecordatorio.IdTarea = int.Parse(dt.Rows[i]["fk_id_tarea"].ToString());
                auxListaRecordatorios.Add(auxRecordatorio);
            }

            return auxListaRecordatorios;
        } //Fin Listar Recordatorios del dia
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaConexion;
using CapaDTO;

namespace CapaNegocio
{
    public class NegocioTarea
    {
        private ConexionSQLite conexion;

        public ConexionSQLite Conexion { get => conexion; set => conexion = value; }

        public void configurarConexion()
        {
            this.Conexion = new ConexionSQLite();
            this.Conexion.NombreBaseDatos = "GestorTareasPE_DB";
            this.Conexion.NombreTabla = "tarea";
            this.Conexion.CadenaConexion = @"Data Source=..\..\..\CapaConexion\GestorTareasPE_DB.db;";
        } //Fin Configurar Conexion

        public void insertarTarea(Tarea tarea)
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "INSERT INTO " + this.Conexion.NombreTabla +
                                    " (id_tarea, nombre_tarea, detalle_tarea, fecha_creacion_tarea, fecha_modificacion_tarea, prioridad_tarea, repetible, ultima_repeticion, estado_tarea) " +
                                    "VALUES (null," +
                                            "'" + tarea.NombreTarea + "'," +
                                            "'" + tarea.DetalleTarea + "'," +
                                            "datetime(CURRENT_TIMESTAMP, 'localtime')," +
                                            "'" + tarea.FechaModificacionTarea + "'," +
                                            tarea.PrioridadTarea + "," +
                                            tarea.Repetible + "," +
                                            "datetime(CURRENT_TIMESTAMP, 'localtime')," +
                                            tarea.EstadoTarea + ");";
            this.Conexion.EsSelect = false;
            this.Conexion.conectar();
        } //Fin Insertar Tarea

        public void eliminarTarea(int idTarea)
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "DELETE FROM " + this.Conexion.NombreTabla
                                   + " WHERE id_tarea = " + idTarea + ";";
            this.Conexion.EsSelect = false;
            this.Conexion.conectar();
        } //Fin Eliminar Tarea

        public void modificarTarea(Tarea tarea)
        {
            this.configurarConexion();

            int intEstadoTarea;
            if(tarea.EstadoTarea == false)
            {
                intEstadoTarea = 0;
            }
            else
            {
                intEstadoTarea = 1;
            }

            int intPrioridad;
            if (tarea.PrioridadTarea == false)
            {
                intPrioridad = 0;
            }
            else
            {
                intPrioridad = 1;
            }

            int intRepetible;
            if (tarea.Repetible == false)
            {
                intRepetible = 0;
            }
            else
            {
                intRepetible = 1;
            }

            this.Conexion.CadenaSQL = "UPDATE " + this.Conexion.NombreTabla +
                                   " SET" +
                                   " nombre_tarea = '" + tarea.NombreTarea + "'," +
                                   " detalle_tarea = '" + tarea.DetalleTarea + "'," +
                                   " fecha_modificacion_tarea = datetime(CURRENT_TIMESTAMP, 'localtime')," +
                                   " prioridad_tarea = " + intPrioridad + "," +
                                   " repetible = " + intRepetible + "," +
                                   " estado_tarea = " + intEstadoTarea +
                                   " WHERE id_tarea = " + tarea.IdTarea + ";";
            this.Conexion.EsSelect = false;
            this.Conexion.conectar();
        } //Fin Modificar Tarea

        public void repetirTarea(Tarea tarea)
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "UPDATE " + this.Conexion.NombreTabla +
                                   " SET" +
                                   " ultima_repeticion = datetime(CURRENT_TIMESTAMP, 'localtime')" +
                                   " WHERE id_tarea = " + tarea.IdTarea + ";";
            this.Conexion.EsSelect = false;
            this.Conexion.conectar();
        } //Fin Repetir Tarea

        public List<Tarea> listarTareas()
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "SELECT * FROM " + this.Conexion.NombreTabla + ";";
            this.Conexion.EsSelect = true;
            this.Conexion.conectar();

            DataTable dt = new DataTable();
            dt = this.Conexion.DbDataSet.Tables[this.Conexion.NombreTabla];

            List<Tarea> auxListaTareas = new List<Tarea>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Tarea auxTarea = new Tarea();
                auxTarea.IdTarea = int.Parse(dt.Rows[i]["id_tarea"].ToString());
                auxTarea.NombreTarea = (String)dt.Rows[i]["nombre_tarea"];
                auxTarea.DetalleTarea = (String)dt.Rows[i]["detalle_tarea"];
                auxTarea.FechaCreacionTarea = Convert.ToDateTime(dt.Rows[i]["fecha_creacion_tarea"]);
                auxTarea.FechaModificacionTarea = Convert.ToDateTime(dt.Rows[i]["fecha_modificacion_tarea"]);
                auxTarea.PrioridadTarea = Convert.ToBoolean(dt.Rows[i]["prioridad_tarea"]);
                auxTarea.Repetible = Convert.ToBoolean(dt.Rows[i]["repetible"]);
                auxTarea.EstadoTarea = Convert.ToBoolean(dt.Rows[i]["estado_tarea"]);
                auxListaTareas.Add(auxTarea);
            }

            return auxListaTareas;
        } //Fin Listar todas las tareas

        public List<Tarea> listarTareasEstado(int idEstado)
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "SELECT * FROM " + this.Conexion.NombreTabla +
                                      " WHERE estado_tarea="+idEstado+"" +
                                      " ORDER BY prioridad_tarea DESC, fecha_creacion_tarea DESC;";
            this.Conexion.EsSelect = true;
            this.Conexion.conectar();

            DataTable dt = new DataTable();
            dt = this.Conexion.DbDataSet.Tables[this.Conexion.NombreTabla];

            List<Tarea> auxListaTareas = new List<Tarea>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Tarea auxTarea = new Tarea();
                auxTarea.IdTarea = int.Parse(dt.Rows[i]["id_tarea"].ToString());
                auxTarea.NombreTarea = (String)dt.Rows[i]["nombre_tarea"];
                auxTarea.DetalleTarea = (String)dt.Rows[i]["detalle_tarea"];
                auxTarea.FechaCreacionTarea = Convert.ToDateTime(dt.Rows[i]["fecha_creacion_tarea"]);
                auxTarea.FechaModificacionTarea = Convert.ToDateTime(dt.Rows[i]["fecha_modificacion_tarea"]);
                auxTarea.PrioridadTarea = Convert.ToBoolean(dt.Rows[i]["prioridad_tarea"]);
                auxTarea.Repetible = Convert.ToBoolean(dt.Rows[i]["repetible"]);
                auxTarea.EstadoTarea = Convert.ToBoolean(dt.Rows[i]["estado_tarea"]);
                auxListaTareas.Add(auxTarea);
            }

            return auxListaTareas;
        } //Fin Listar tareas por estado

        public List<Tarea> listarTareasRepetibles()
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "SELECT * FROM " + this.Conexion.NombreTabla +
                                      " WHERE estado_tarea=1" +
                                      " AND repetible=1;";
            this.Conexion.EsSelect = true;
            this.Conexion.conectar();

            DataTable dt = new DataTable();
            dt = this.Conexion.DbDataSet.Tables[this.Conexion.NombreTabla];

            List<Tarea> auxListaTareas = new List<Tarea>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Tarea auxTarea = new Tarea();
                auxTarea.IdTarea = int.Parse(dt.Rows[i]["id_tarea"].ToString());
                auxTarea.NombreTarea = (String)dt.Rows[i]["nombre_tarea"];
                auxTarea.DetalleTarea = (String)dt.Rows[i]["detalle_tarea"];
                auxTarea.FechaCreacionTarea = Convert.ToDateTime(dt.Rows[i]["fecha_creacion_tarea"]);
                auxTarea.FechaModificacionTarea = Convert.ToDateTime(dt.Rows[i]["fecha_modificacion_tarea"]);
                auxTarea.PrioridadTarea = Convert.ToBoolean(dt.Rows[i]["prioridad_tarea"]);
                auxTarea.Repetible = Convert.ToBoolean(dt.Rows[i]["repetible"]);
                auxTarea.UltimaRepeticion = Convert.ToDateTime(dt.Rows[i]["ultima_repeticion"]);
                auxTarea.EstadoTarea = Convert.ToBoolean(dt.Rows[i]["estado_tarea"]);
                auxListaTareas.Add(auxTarea);
            }

            return auxListaTareas;
        } //Fin Listar tareas por estado

        public List<Tarea> listarTareasFiltro(string texto, int fecha, int estado)
        {
            string fechaLimite;

            switch (fecha)
            {
                case 1: //Hoy
                    fechaLimite = "-24 hours";
                    break;
                case 2: //7 dias
                    fechaLimite = "-7 days";
                    break;
                case 3: //30 dias
                    fechaLimite = "-30 days";
                    break;
                default:
                    fechaLimite = "-10 years";
                    break;
            }

            string whereFecha = " AND (fecha_creacion_tarea > datetime(CURRENT_TIMESTAMP, '" + fechaLimite + "', 'localtime'))";
            string whereEstado = " AND (estado_tarea=" + estado + ")";

            if (estado > 1)
            {
                whereEstado = "";
            }

            this.configurarConexion();
            this.Conexion.CadenaSQL = "SELECT * FROM " + this.Conexion.NombreTabla +
                                      " WHERE (nombre_tarea LIKE '%" + texto + "%'" +
                                      " OR detalle_tarea LIKE '%" + texto + "%')" +
                                      whereFecha +
                                      whereEstado +
                                      "; ";
            this.Conexion.EsSelect = true;
            this.Conexion.conectar();

            DataTable dt = new DataTable();
            dt = this.Conexion.DbDataSet.Tables[this.Conexion.NombreTabla];

            List<Tarea> auxListaTareas = new List<Tarea>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Tarea auxTarea = new Tarea();
                auxTarea.IdTarea = int.Parse(dt.Rows[i]["id_tarea"].ToString());
                auxTarea.NombreTarea = (String)dt.Rows[i]["nombre_tarea"];
                auxTarea.DetalleTarea = (String)dt.Rows[i]["detalle_tarea"];
                auxTarea.FechaCreacionTarea = Convert.ToDateTime(dt.Rows[i]["fecha_creacion_tarea"]);
                auxTarea.FechaModificacionTarea = Convert.ToDateTime(dt.Rows[i]["fecha_modificacion_tarea"]);
                auxTarea.PrioridadTarea = Convert.ToBoolean(dt.Rows[i]["prioridad_tarea"]);
                auxTarea.Repetible = Convert.ToBoolean(dt.Rows[i]["repetible"]);
                auxTarea.EstadoTarea = Convert.ToBoolean(dt.Rows[i]["estado_tarea"]);
                auxListaTareas.Add(auxTarea);
            }

            return auxListaTareas;
        } //Fin Listar tareas por filtro

        public Tarea buscarTarea(int idTarea)
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "SELECT * FROM " + this.Conexion.NombreTabla +
                                      " WHERE id_tarea = " + idTarea + ";";
            this.Conexion.EsSelect = true;
            this.Conexion.conectar();

            Tarea auxTarea = new Tarea();
            DataTable dt = new DataTable();
            dt = this.Conexion.DbDataSet.Tables[this.Conexion.NombreTabla];

            try
            {
                auxTarea.IdTarea = int.Parse(dt.Rows[0]["id_tarea"].ToString());
                auxTarea.NombreTarea = (String)dt.Rows[0]["nombre_tarea"];
                auxTarea.DetalleTarea = (String)dt.Rows[0]["detalle_tarea"];
                auxTarea.FechaCreacionTarea = Convert.ToDateTime(dt.Rows[0]["fecha_creacion_tarea"]);
                auxTarea.FechaModificacionTarea = Convert.ToDateTime(dt.Rows[0]["fecha_modificacion_tarea"]);
                auxTarea.PrioridadTarea = Convert.ToBoolean(dt.Rows[0]["prioridad_tarea"]);
                auxTarea.Repetible = Convert.ToBoolean(dt.Rows[0]["repetible"]);
                auxTarea.EstadoTarea = Convert.ToBoolean(dt.Rows[0]["estado_tarea"]);
                
            }
            catch(Exception ex)
            {
                auxTarea.IdTarea = 0;
                auxTarea.NombreTarea = String.Empty;
            }

            return auxTarea;
        } //Fin Buscar Tarea

        public int obtenerUltimoId()
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "SELECT MAX(id_tarea) as id_tarea FROM tarea;";
            this.Conexion.EsSelect = true;
            this.Conexion.conectar();


            int ultimoIdTarea;
            DataTable dt = new DataTable();
            dt = this.Conexion.DbDataSet.Tables[this.Conexion.NombreTabla];

            try
            {
                ultimoIdTarea = int.Parse(dt.Rows[0]["id_tarea"].ToString());
            }
            catch (Exception ex)
            {
                ultimoIdTarea = 0;
            }

            return ultimoIdTarea;
        } //Fin Obtener Ultimo Id

    }
}
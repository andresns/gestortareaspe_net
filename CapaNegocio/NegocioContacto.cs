﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioContacto
    {
        private ConexionSQLite conexion;

        public ConexionSQLite Conexion { get => conexion; set => conexion = value; }

        public void configurarConexion()
        {
            this.Conexion = new ConexionSQLite();
            this.Conexion.NombreBaseDatos = "GestorTareasPE_DB";
            this.Conexion.NombreTabla = "contacto";
            this.Conexion.CadenaConexion = @"Data Source=..\..\..\CapaConexion\GestorTareasPE_DB.db;";
        } //Fin Configurar Conexion

        public void insertarContacto(Contacto contacto)
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "INSERT INTO " + this.Conexion.NombreTabla +
                                    " (id_contacto, nombre_contacto, telefono_contacto) " +
                                    "VALUES (null," +
                                            "'" + contacto.NombreContacto + "'," +
                                            "'" + contacto.TelefonoContacto + "');";
            this.Conexion.EsSelect = false;
            this.Conexion.conectar();
        } //Fin Insertar Contacto

        public void eliminarContacto(int idContacto)
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "DELETE FROM " + this.Conexion.NombreTabla
                                   + " WHERE id_contacto = " + idContacto + ";";
            this.Conexion.EsSelect = false;
            this.Conexion.conectar();
        } //Fin Eliminar Contacto

        public void modificarContacto(Contacto contacto)
        {
            this.configurarConexion();
            
            this.Conexion.CadenaSQL = "UPDATE " + this.Conexion.NombreTabla +
                                   " SET" +
                                   " nombre_contacto = '" + contacto.NombreContacto + "'," +
                                   " telefono_contacto = '" + contacto.TelefonoContacto + "'" +
                                   " WHERE id_contacto = " + contacto.IdContacto + ";";
            this.Conexion.EsSelect = false;
            this.Conexion.conectar();
        } //Fin Modificar Contacto

        public Contacto buscarContacto(int idContacto)
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "SELECT * FROM " + this.Conexion.NombreTabla +
                                      " WHERE id_contacto = " + idContacto + ";";
            this.Conexion.EsSelect = true;
            this.Conexion.conectar();

            Contacto auxContacto = new Contacto();
            DataTable dt = new DataTable();
            dt = this.Conexion.DbDataSet.Tables[this.Conexion.NombreTabla];

            try
            {
                auxContacto.IdContacto = int.Parse(dt.Rows[0]["id_Contacto"].ToString());
                auxContacto.NombreContacto = (String)dt.Rows[0]["nombre_contacto"];
                auxContacto.TelefonoContacto = (String)dt.Rows[0]["telefono_contacto"];
            }
            catch (Exception ex)
            {
                auxContacto.IdContacto = 0;
                auxContacto.NombreContacto = String.Empty;
            }

            return auxContacto;
        }

        public List<Contacto> listarContactos()
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "SELECT * FROM " + this.Conexion.NombreTabla + ";";
            this.Conexion.EsSelect = true;
            this.Conexion.conectar();

            DataTable dt = new DataTable();
            dt = this.Conexion.DbDataSet.Tables[this.Conexion.NombreTabla];

            List<Contacto> auxListaContactos = new List<Contacto>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Contacto auxContacto = new Contacto();
                auxContacto.IdContacto = int.Parse(dt.Rows[i]["id_contacto"].ToString());
                auxContacto.NombreContacto = (String)dt.Rows[i]["nombre_contacto"];
                auxContacto.TelefonoContacto = (String)dt.Rows[i]["telefono_contacto"];
                auxListaContactos.Add(auxContacto);
            }

            return auxListaContactos;
        } //Fin Listar Contactos

        public List<Contacto> buscarContactos(string textoBusqueda)
        {
            this.configurarConexion();
            this.Conexion.CadenaSQL = "SELECT * FROM " + this.Conexion.NombreTabla +
                                      " WHERE nombre_contacto LIKE '%" + textoBusqueda + "%';";
            this.Conexion.EsSelect = true;
            this.Conexion.conectar();

            DataTable dt = new DataTable();
            dt = this.Conexion.DbDataSet.Tables[this.Conexion.NombreTabla];

            List<Contacto> auxListaContactos = new List<Contacto>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Contacto auxContacto = new Contacto();
                auxContacto.IdContacto = int.Parse(dt.Rows[i]["id_contacto"].ToString());
                auxContacto.NombreContacto = (String)dt.Rows[i]["nombre_contacto"];
                auxContacto.TelefonoContacto = (String)dt.Rows[i]["telefono_contacto"];
                auxListaContactos.Add(auxContacto);
            }

            return auxListaContactos;
        } //Fin Buscar Contacto
    }
}

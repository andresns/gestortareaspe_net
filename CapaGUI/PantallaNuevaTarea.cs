﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using CapaDTO;

namespace CapaGUI
{
    public partial class PantallaNuevaTarea : Form
    {
        #region VARIABLES DE INSTANCIA, PROPERTIES

        private PantallaInicio pInicio;
        private bool estadoBtnPrioridad;
        private bool estadoBtnRepetirTarea;
        private bool estadoBtnRecordatorio;
        private bool pnlRecordatorioOculto;
        private bool placeholderNombreTarea;
        private bool placeholderDetalleTarea;

        /********** PROPERTIES ***********/
        public PantallaInicio PInicio { get => pInicio; set => pInicio = value; }
        public bool EstadoBtnPrioridad { get => estadoBtnPrioridad; set => estadoBtnPrioridad = value; }
        public bool EstadoBtnRepetirTarea { get => estadoBtnRepetirTarea; set => estadoBtnRepetirTarea = value; }
        public bool EstadoBtnRecordatorio { get => estadoBtnRecordatorio; set => estadoBtnRecordatorio = value; }
        public bool PnlRecordatorioOculto { get => pnlRecordatorioOculto; set => pnlRecordatorioOculto = value; }
        public bool PlaceholderNombreTarea { get => placeholderNombreTarea; set => placeholderNombreTarea = value; }
        public bool PlaceholderDetalleTarea { get => placeholderDetalleTarea; set => placeholderDetalleTarea = value; }

        //Constantes necesarias para arrastrar desde panel (Barra de titulo)
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture(); //Fin mover pantalla desde barra de titulo

        #endregion VARIABLES DE INSTANCIA, PROPERTIES


        /********** INICIALIZACION ***********/
        public PantallaNuevaTarea(PantallaInicio pInicio, string nombreTarea)
        {
            InitializeComponent();
            this.PInicio = pInicio;

            if (nombreTarea != "")
            {
                this.txtNombreTarea.Text = nombreTarea;
                this.txtNombreTarea.ForeColor = Color.Black;
                this.PlaceholderNombreTarea = false;
                this.txtDetalleTarea.Select();
                this.txtDetalleTarea.Text = String.Empty;
                this.txtDetalleTarea.ForeColor = Color.Black;
                this.PlaceholderDetalleTarea = false;
            }
            else
            {
                this.PlaceholderNombreTarea = true;
                this.PlaceholderDetalleTarea = true;
            }
            
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PantallaNuevaTarea_FormClosing);
        }

        //TO DO - Deshabilitar lo que esta dentro del panel de recordatorio y habilitarlos en el evento click
        private void PantallaNuevaTarea_Load(object sender, EventArgs e)
        {
            this.EstadoBtnPrioridad = false;
            this.EstadoBtnRepetirTarea = false;
            this.EstadoBtnRecordatorio = false;
            this.PnlRecordatorioOculto = true;
            
        }


        #region BOTONES

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtNombreTarea.Text == String.Empty)
            {
                MessageBox.Show("Debe selecionar un nombre para la tarea.", "Sistema");
            }
            else
            {
                NegocioTarea auxNegocioTarea = new NegocioTarea();
                Tarea auxTarea = new Tarea();

                auxTarea.NombreTarea = txtNombreTarea.Text;
                if (PlaceholderDetalleTarea)
                {
                    txtDetalleTarea.Text = String.Empty;
                    auxTarea.DetalleTarea = txtDetalleTarea.Text;
                }
                else
                {
                    auxTarea.DetalleTarea = txtDetalleTarea.Text;
                }

                auxTarea.EstadoTarea = false;
                auxTarea.PrioridadTarea = EstadoBtnPrioridad;
                auxTarea.Repetible = EstadoBtnRepetirTarea;

                auxNegocioTarea.insertarTarea(auxTarea);

                //Agregar recordatorio
                if (EstadoBtnRecordatorio)
                {
                    NegocioRecordatorio auxNegocioRecordatorio = new NegocioRecordatorio();
                    Recordatorio auxRecordatorio = new Recordatorio();

                    DateTime auxDiaRecordatorio = this.dtpDiaRecordatorio.Value;
                    DateTime auxHoraRecordatorio = this.dtpHoraRecordatorio.Value;
                    DateTime auxFechaRecordatorio = auxDiaRecordatorio.Date + auxHoraRecordatorio.TimeOfDay; //Quitar segundos

                    auxRecordatorio.FechaRecordatorio = auxFechaRecordatorio;
                    auxRecordatorio.RepetirRecordatorio = false; //Por defecto quedará false basado en la nueva GUI
                    auxRecordatorio.IdTarea = auxNegocioTarea.obtenerUltimoId();

                    auxNegocioRecordatorio.insertarRecordatorio(auxRecordatorio);
                }

                this.Dispose();
                System.GC.Collect();
                this.PInicio.cargarRecordatorios();
            }

        }//Fin Guardar Click

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose(true);
            System.GC.Collect();
        }//Fin Cancelar Click

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Dispose();
            System.GC.Collect();
        }//Fin Cerrar Click

        private void btnPrioridad_Click(object sender, EventArgs e)
        {
            if (EstadoBtnPrioridad)
            {
                this.btnPrioridad.Image = Properties.Resources.star_borde_amarillo;
                this.btnPrioridad.ForeColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnPrioridad.BackColor = Color.White;
                this.btnPrioridad.FlatAppearance.MouseDownBackColor = this.btnPrioridad.BackColor;
                this.btnPrioridad.FlatAppearance.MouseOverBackColor = this.btnPrioridad.BackColor;
                this.EstadoBtnPrioridad = false;
            }
            else
            {
                this.btnPrioridad.Image = Properties.Resources.star;
                this.btnPrioridad.ForeColor = Color.White;
                this.btnPrioridad.BackColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnPrioridad.FlatAppearance.MouseDownBackColor = this.btnPrioridad.BackColor;
                this.btnPrioridad.FlatAppearance.MouseOverBackColor = this.btnPrioridad.BackColor;
                this.EstadoBtnPrioridad = true;
            }
        }//Fin Boton Prioridad Click

        private void btnRepetirTarea_Click(object sender, EventArgs e)
        {
            if (estadoBtnRepetirTarea)
            {
                this.btnRepetirTarea.Image = Properties.Resources.repeat_color;
                this.btnRepetirTarea.ForeColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnRepetirTarea.BackColor = Color.White;
                this.btnRepetirTarea.FlatAppearance.MouseDownBackColor = this.btnRepetirTarea.BackColor;
                this.btnRepetirTarea.FlatAppearance.MouseOverBackColor = this.btnRepetirTarea.BackColor;
                this.EstadoBtnRepetirTarea = false;
            }
            else
            {
                this.btnRepetirTarea.Image = Properties.Resources.repeat;
                this.btnRepetirTarea.ForeColor = Color.White;
                this.btnRepetirTarea.BackColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnRepetirTarea.FlatAppearance.MouseDownBackColor = this.btnRepetirTarea.BackColor;
                this.btnRepetirTarea.FlatAppearance.MouseOverBackColor = this.btnRepetirTarea.BackColor;
                this.EstadoBtnRepetirTarea = true;
            }
        }//Fin Boton Repetir Tarea click

        private void btnRecordatorio_Click(object sender, EventArgs e)
        {
            if (EstadoBtnRecordatorio)
            {
                this.btnRecordatorio.Image = Properties.Resources.recordatorio_color;
                this.btnRecordatorio.ForeColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnRecordatorio.BackColor = Color.White;
                this.btnRecordatorio.FlatAppearance.MouseDownBackColor = this.btnRecordatorio.BackColor;
                this.btnRecordatorio.FlatAppearance.MouseOverBackColor = this.btnRecordatorio.BackColor;
                this.EstadoBtnRecordatorio = false;
                this.tmrAnimRecordatorio.Start();
            }
            else
            {
                this.btnRecordatorio.Image = Properties.Resources.recordatorio;
                this.btnRecordatorio.ForeColor = Color.White;
                this.btnRecordatorio.BackColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnRecordatorio.FlatAppearance.MouseDownBackColor = this.btnRecordatorio.BackColor;
                this.btnRecordatorio.FlatAppearance.MouseOverBackColor = this.btnRecordatorio.BackColor;
                this.EstadoBtnRecordatorio = true;
                this.tmrAnimRecordatorio.Start();
            }
        }//Fin Boton Recordatorio Click

        #endregion BOTONES

        #region EVENTOS

        /********** EVENTOS ON CHANGE ***********/
        private void PantallaNuevaTarea_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.PInicio.cargarTareasPendientes();
        }

        private void txtNombreTarea_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnGuardar_Click(sender, e);

            }

            if (e.KeyCode == Keys.Escape)
            {
                btnCancelar_Click(sender, e);
            }
        } //Fin Hotkeys Nombre Tarea

        private void txtDetalleTarea_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancelar_Click(sender, e);
            }
        } //Fin Hotkeys Detalle Tarea

        #endregion EVENTOS

        #region GUI

        /********** EFECTOS HOVER ***********/
        private void btnPrioridad_MouseEnter(object sender, EventArgs e)
        {
            Button auxButton = (Button)sender;
            auxButton.FlatAppearance.MouseOverBackColor = this.btnPrioridad.BackColor;
            auxButton.FlatAppearance.MouseDownBackColor = this.btnPrioridad.BackColor;
        }

        private void btnRepetirTarea_MouseEnter(object sender, EventArgs e)
        {
            Button auxButton = (Button)sender;
            auxButton.FlatAppearance.MouseOverBackColor = this.btnRepetirTarea.BackColor;
            auxButton.FlatAppearance.MouseDownBackColor = this.btnRepetirTarea.BackColor;
        }

        private void btnRecordatorio_MouseEnter(object sender, EventArgs e)
        {
            Button auxButton = (Button)sender;
            auxButton.FlatAppearance.MouseOverBackColor = this.btnRecordatorio.BackColor;
            auxButton.FlatAppearance.MouseDownBackColor = this.btnRecordatorio.BackColor;
        }


        /********** ANIMACION PANEL RECORDATORIO ***********/
        private void tmrAnimRecordatorio_Tick(object sender, EventArgs e)
        {
            if (PnlRecordatorioOculto)
            {
                this.pnlRecordatorio.Height = this.pnlRecordatorio.Height + 10;
                this.btnCancelar.Location = new Point(this.btnCancelar.Location.X, this.btnCancelar.Location.Y + 10);
                this.btnGuardar.Location = new Point(this.btnGuardar.Location.X, this.btnGuardar.Location.Y + 10);
                this.Height = this.Height + 10;

                if (this.pnlRecordatorio.Height >= 110)
                {
                    this.tmrAnimRecordatorio.Stop();
                    this.Refresh();
                    this.PnlRecordatorioOculto = false;
                }
            }
            else
            {
                this.pnlRecordatorio.Height = this.pnlRecordatorio.Height - 10;
                this.btnCancelar.Location = new Point(this.btnCancelar.Location.X, this.btnCancelar.Location.Y - 10);
                this.btnGuardar.Location = new Point(this.btnGuardar.Location.X, this.btnGuardar.Location.Y - 10);
                this.Height = this.Height - 10;

                if (this.pnlRecordatorio.Height <= 0)
                {
                    this.tmrAnimRecordatorio.Stop();
                    this.Refresh();
                    this.PnlRecordatorioOculto = true;
                }
            }
        }

        /************ PLACEHOLDER ************/
        private void txtNombreTarea_Enter(object sender, EventArgs e)
        {
            if (this.txtNombreTarea.Text == "Tarea" && PlaceholderNombreTarea)
            {
                this.txtNombreTarea.Text = String.Empty;
                this.txtNombreTarea.ForeColor = Color.Black;
            }
        }

        private void txtNombreTarea_Leave(object sender, EventArgs e)
        {
            if (this.txtNombreTarea.Text == String.Empty)
            {
                this.txtNombreTarea.Text = "Tarea";
                this.txtNombreTarea.ForeColor = Color.DarkGray;
                PlaceholderNombreTarea = true;
            }
        }

        private void txtNombreTarea_TextChanged(object sender, EventArgs e)
        {
            if (this.txtNombreTarea.Text == String.Empty && PlaceholderNombreTarea)
            {
                PlaceholderNombreTarea = false;
            }
        }

        private void txtDetalleTarea_Enter(object sender, EventArgs e)
        {
            if (this.txtDetalleTarea.Text == "Detalle" && PlaceholderDetalleTarea)
            {
                this.txtDetalleTarea.Text = String.Empty;
                this.txtDetalleTarea.ForeColor = Color.Black;
            }
        }

        private void txtDetalleTarea_Leave(object sender, EventArgs e)
        {
            if (this.txtDetalleTarea.Text == String.Empty)
            {
                this.txtDetalleTarea.Text = "Detalle";
                this.txtDetalleTarea.ForeColor = Color.DarkGray;
                PlaceholderDetalleTarea = true;
            }
        }

        private void txtDetalleTarea_TextChanged(object sender, EventArgs e)
        {
            if (this.txtDetalleTarea.Text == String.Empty && PlaceholderDetalleTarea)
            {
                PlaceholderDetalleTarea = false;
            }
        }

        /************ MOVER VENTANA DESDE BARRA DE TITULO ************/
        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void lblTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        #endregion GUI


    }
}

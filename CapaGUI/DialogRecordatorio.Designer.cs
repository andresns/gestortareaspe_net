﻿namespace CapaGUI
{
    partial class DialogRecordatorio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DialogRecordatorio));
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnVerTarea = new System.Windows.Forms.Button();
            this.lblNombreTarea = new System.Windows.Forms.Label();
            this.cbxPostergarRecordatorio = new System.Windows.Forms.ComboBox();
            this.chkPostergarRecordatorio = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAceptar
            // 
            this.btnAceptar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(96)))), ((int)(((byte)(100)))));
            this.btnAceptar.FlatAppearance.BorderSize = 0;
            this.btnAceptar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(142)))), ((int)(((byte)(146)))));
            this.btnAceptar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(142)))), ((int)(((byte)(146)))));
            this.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAceptar.Font = new System.Drawing.Font("Montserrat Medium", 8F);
            this.btnAceptar.ForeColor = System.Drawing.Color.White;
            this.btnAceptar.Location = new System.Drawing.Point(207, 92);
            this.btnAceptar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(119, 32);
            this.btnAceptar.TabIndex = 3;
            this.btnAceptar.Text = "ACEPTAR";
            this.btnAceptar.UseVisualStyleBackColor = false;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnVerTarea
            // 
            this.btnVerTarea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(96)))), ((int)(((byte)(100)))));
            this.btnVerTarea.FlatAppearance.BorderSize = 0;
            this.btnVerTarea.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(142)))), ((int)(((byte)(146)))));
            this.btnVerTarea.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVerTarea.Font = new System.Drawing.Font("Montserrat Medium", 8F);
            this.btnVerTarea.ForeColor = System.Drawing.Color.White;
            this.btnVerTarea.Location = new System.Drawing.Point(84, 92);
            this.btnVerTarea.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnVerTarea.Name = "btnVerTarea";
            this.btnVerTarea.Size = new System.Drawing.Size(119, 32);
            this.btnVerTarea.TabIndex = 2;
            this.btnVerTarea.Text = "VER TAREA";
            this.btnVerTarea.UseVisualStyleBackColor = false;
            this.btnVerTarea.Click += new System.EventHandler(this.btnVerTarea_Click);
            // 
            // lblNombreTarea
            // 
            this.lblNombreTarea.Font = new System.Drawing.Font("Montserrat Medium", 8F);
            this.lblNombreTarea.ForeColor = System.Drawing.Color.White;
            this.lblNombreTarea.Location = new System.Drawing.Point(0, 0);
            this.lblNombreTarea.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNombreTarea.Name = "lblNombreTarea";
            this.lblNombreTarea.Size = new System.Drawing.Size(333, 45);
            this.lblNombreTarea.TabIndex = 4;
            this.lblNombreTarea.Text = "Nombre de Tarea";
            this.lblNombreTarea.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxPostergarRecordatorio
            // 
            this.cbxPostergarRecordatorio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxPostergarRecordatorio.FormattingEnabled = true;
            this.cbxPostergarRecordatorio.Items.AddRange(new object[] {
            "en 15 minutos",
            "en 30 minutos",
            "en 1 hora",
            "Mañana"});
            this.cbxPostergarRecordatorio.Location = new System.Drawing.Point(155, 62);
            this.cbxPostergarRecordatorio.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxPostergarRecordatorio.Name = "cbxPostergarRecordatorio";
            this.cbxPostergarRecordatorio.Size = new System.Drawing.Size(171, 21);
            this.cbxPostergarRecordatorio.TabIndex = 1;
            // 
            // chkPostergarRecordatorio
            // 
            this.chkPostergarRecordatorio.AutoSize = true;
            this.chkPostergarRecordatorio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.chkPostergarRecordatorio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chkPostergarRecordatorio.Font = new System.Drawing.Font("Montserrat Medium", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkPostergarRecordatorio.Location = new System.Drawing.Point(8, 62);
            this.chkPostergarRecordatorio.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkPostergarRecordatorio.Name = "chkPostergarRecordatorio";
            this.chkPostergarRecordatorio.Size = new System.Drawing.Size(140, 19);
            this.chkPostergarRecordatorio.TabIndex = 0;
            this.chkPostergarRecordatorio.Text = "Volver a recordarme";
            this.chkPostergarRecordatorio.UseVisualStyleBackColor = true;
            this.chkPostergarRecordatorio.CheckedChanged += new System.EventHandler(this.chkPostergarRecordatorio_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(54)))), ((int)(((byte)(58)))));
            this.panel1.Controls.Add(this.lblNombreTarea);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(333, 45);
            this.panel1.TabIndex = 5;
            // 
            // DialogRecordatorio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(333, 135);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.chkPostergarRecordatorio);
            this.Controls.Add(this.cbxPostergarRecordatorio);
            this.Controls.Add(this.btnVerTarea);
            this.Controls.Add(this.btnAceptar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "DialogRecordatorio";
            this.ShowInTaskbar = false;
            this.Text = "DialogRecordatorio";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.DialogRecordatorio_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Button btnVerTarea;
        private System.Windows.Forms.Label lblNombreTarea;
        private System.Windows.Forms.ComboBox cbxPostergarRecordatorio;
        private System.Windows.Forms.CheckBox chkPostergarRecordatorio;
        private System.Windows.Forms.Panel panel1;
    }
}
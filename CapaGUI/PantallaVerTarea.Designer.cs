﻿namespace CapaGUI
{
    partial class PantallaVerTarea
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PantallaVerTarea));
            this.pnlRecordatorio = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dtpDiaRecordatorio = new System.Windows.Forms.DateTimePicker();
            this.dtpHoraRecordatorio = new System.Windows.Forms.DateTimePicker();
            this.btnRecordatorio = new System.Windows.Forms.Button();
            this.btnRepetirTarea = new System.Windows.Forms.Button();
            this.btnPrioridad = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtDetalleTarea = new System.Windows.Forms.TextBox();
            this.pnlTxtNombreTarea = new System.Windows.Forms.Panel();
            this.txtNombreTarea = new System.Windows.Forms.TextBox();
            this.btnEliminarTarea = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.lblTitleBar = new System.Windows.Forms.Label();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.tmrAnimRecordatorio = new System.Windows.Forms.Timer(this.components);
            this.btnEstadoTarea = new System.Windows.Forms.Button();
            this.pnlRecordatorio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.pnlTxtNombreTarea.SuspendLayout();
            this.pnlTitleBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlRecordatorio
            // 
            this.pnlRecordatorio.BackColor = System.Drawing.Color.White;
            this.pnlRecordatorio.Controls.Add(this.pictureBox2);
            this.pnlRecordatorio.Controls.Add(this.pictureBox1);
            this.pnlRecordatorio.Controls.Add(this.dtpDiaRecordatorio);
            this.pnlRecordatorio.Controls.Add(this.dtpHoraRecordatorio);
            this.pnlRecordatorio.Location = new System.Drawing.Point(24, 462);
            this.pnlRecordatorio.Margin = new System.Windows.Forms.Padding(15, 0, 15, 15);
            this.pnlRecordatorio.Name = "pnlRecordatorio";
            this.pnlRecordatorio.Size = new System.Drawing.Size(503, 0);
            this.pnlRecordatorio.TabIndex = 23;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::CapaGUI.Properties.Resources.clock_color;
            this.pictureBox2.Location = new System.Drawing.Point(30, 60);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(32, 32);
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CapaGUI.Properties.Resources.calendar_color;
            this.pictureBox1.Location = new System.Drawing.Point(30, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(30, 15, 2, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // dtpDiaRecordatorio
            // 
            this.dtpDiaRecordatorio.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtpDiaRecordatorio.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.dtpDiaRecordatorio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDiaRecordatorio.Location = new System.Drawing.Point(82, 14);
            this.dtpDiaRecordatorio.Margin = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.dtpDiaRecordatorio.Name = "dtpDiaRecordatorio";
            this.dtpDiaRecordatorio.Size = new System.Drawing.Size(187, 33);
            this.dtpDiaRecordatorio.TabIndex = 3;
            // 
            // dtpHoraRecordatorio
            // 
            this.dtpHoraRecordatorio.CustomFormat = "HH:mm";
            this.dtpHoraRecordatorio.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.dtpHoraRecordatorio.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpHoraRecordatorio.Location = new System.Drawing.Point(82, 60);
            this.dtpHoraRecordatorio.Margin = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.dtpHoraRecordatorio.Name = "dtpHoraRecordatorio";
            this.dtpHoraRecordatorio.ShowUpDown = true;
            this.dtpHoraRecordatorio.Size = new System.Drawing.Size(187, 33);
            this.dtpHoraRecordatorio.TabIndex = 2;
            // 
            // btnRecordatorio
            // 
            this.btnRecordatorio.BackColor = System.Drawing.Color.White;
            this.btnRecordatorio.FlatAppearance.BorderSize = 0;
            this.btnRecordatorio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRecordatorio.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.btnRecordatorio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnRecordatorio.Image = global::CapaGUI.Properties.Resources.recordatorio_color;
            this.btnRecordatorio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRecordatorio.Location = new System.Drawing.Point(24, 414);
            this.btnRecordatorio.Margin = new System.Windows.Forms.Padding(0);
            this.btnRecordatorio.Name = "btnRecordatorio";
            this.btnRecordatorio.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.btnRecordatorio.Size = new System.Drawing.Size(503, 48);
            this.btnRecordatorio.TabIndex = 20;
            this.btnRecordatorio.Text = "  Agregar Recordatorio";
            this.btnRecordatorio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRecordatorio.UseVisualStyleBackColor = false;
            this.btnRecordatorio.Click += new System.EventHandler(this.btnRecordatorio_Click);
            this.btnRecordatorio.MouseEnter += new System.EventHandler(this.btnRecordatorio_MouseEnter);
            // 
            // btnRepetirTarea
            // 
            this.btnRepetirTarea.BackColor = System.Drawing.Color.White;
            this.btnRepetirTarea.FlatAppearance.BorderSize = 0;
            this.btnRepetirTarea.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRepetirTarea.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.btnRepetirTarea.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnRepetirTarea.Image = global::CapaGUI.Properties.Resources.repeat_color;
            this.btnRepetirTarea.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRepetirTarea.Location = new System.Drawing.Point(24, 351);
            this.btnRepetirTarea.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.btnRepetirTarea.Name = "btnRepetirTarea";
            this.btnRepetirTarea.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.btnRepetirTarea.Size = new System.Drawing.Size(242, 48);
            this.btnRepetirTarea.TabIndex = 18;
            this.btnRepetirTarea.Text = "  Repetir Diariamente";
            this.btnRepetirTarea.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRepetirTarea.UseVisualStyleBackColor = false;
            this.btnRepetirTarea.Click += new System.EventHandler(this.btnRepetirTarea_Click);
            this.btnRepetirTarea.MouseEnter += new System.EventHandler(this.btnRepetirTarea_MouseEnter);
            // 
            // btnPrioridad
            // 
            this.btnPrioridad.BackColor = System.Drawing.Color.White;
            this.btnPrioridad.FlatAppearance.BorderSize = 0;
            this.btnPrioridad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrioridad.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.btnPrioridad.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnPrioridad.Image = global::CapaGUI.Properties.Resources.star_borde_amarillo;
            this.btnPrioridad.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrioridad.Location = new System.Drawing.Point(285, 351);
            this.btnPrioridad.Margin = new System.Windows.Forms.Padding(0, 0, 0, 15);
            this.btnPrioridad.Name = "btnPrioridad";
            this.btnPrioridad.Padding = new System.Windows.Forms.Padding(7, 0, 0, 0);
            this.btnPrioridad.Size = new System.Drawing.Size(242, 48);
            this.btnPrioridad.TabIndex = 19;
            this.btnPrioridad.Text = "  Tarea Importante";
            this.btnPrioridad.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrioridad.UseVisualStyleBackColor = false;
            this.btnPrioridad.Click += new System.EventHandler(this.btnPrioridad_Click);
            this.btnPrioridad.MouseEnter += new System.EventHandler(this.btnPrioridad_MouseEnter);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.txtDetalleTarea);
            this.panel1.Location = new System.Drawing.Point(24, 116);
            this.panel1.Margin = new System.Windows.Forms.Padding(15, 0, 15, 15);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(503, 220);
            this.panel1.TabIndex = 17;
            // 
            // txtDetalleTarea
            // 
            this.txtDetalleTarea.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDetalleTarea.Font = new System.Drawing.Font("Segoe UI", 14.25F);
            this.txtDetalleTarea.ForeColor = System.Drawing.Color.DarkGray;
            this.txtDetalleTarea.Location = new System.Drawing.Point(10, 10);
            this.txtDetalleTarea.Margin = new System.Windows.Forms.Padding(10);
            this.txtDetalleTarea.Multiline = true;
            this.txtDetalleTarea.Name = "txtDetalleTarea";
            this.txtDetalleTarea.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDetalleTarea.Size = new System.Drawing.Size(483, 200);
            this.txtDetalleTarea.TabIndex = 0;
            this.txtDetalleTarea.Text = "Detalle";
            this.txtDetalleTarea.TextChanged += new System.EventHandler(this.txtDetalleTarea_TextChanged);
            this.txtDetalleTarea.Enter += new System.EventHandler(this.txtDetalleTarea_Enter);
            this.txtDetalleTarea.Leave += new System.EventHandler(this.txtDetalleTarea_Leave);
            // 
            // pnlTxtNombreTarea
            // 
            this.pnlTxtNombreTarea.BackColor = System.Drawing.Color.White;
            this.pnlTxtNombreTarea.Controls.Add(this.txtNombreTarea);
            this.pnlTxtNombreTarea.Location = new System.Drawing.Point(24, 55);
            this.pnlTxtNombreTarea.Margin = new System.Windows.Forms.Padding(15, 20, 15, 15);
            this.pnlTxtNombreTarea.Name = "pnlTxtNombreTarea";
            this.pnlTxtNombreTarea.Size = new System.Drawing.Size(440, 46);
            this.pnlTxtNombreTarea.TabIndex = 16;
            // 
            // txtNombreTarea
            // 
            this.txtNombreTarea.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNombreTarea.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreTarea.ForeColor = System.Drawing.Color.DarkGray;
            this.txtNombreTarea.Location = new System.Drawing.Point(15, 10);
            this.txtNombreTarea.Margin = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.txtNombreTarea.Name = "txtNombreTarea";
            this.txtNombreTarea.Size = new System.Drawing.Size(410, 26);
            this.txtNombreTarea.TabIndex = 0;
            this.txtNombreTarea.Text = "Tarea";
            this.txtNombreTarea.TextChanged += new System.EventHandler(this.txtDetalleTarea_TextChanged);
            this.txtNombreTarea.Enter += new System.EventHandler(this.txtNombreTarea_Enter);
            this.txtNombreTarea.Leave += new System.EventHandler(this.txtNombreTarea_Leave);
            // 
            // btnEliminarTarea
            // 
            this.btnEliminarTarea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(0)))), ((int)(((byte)(15)))));
            this.btnEliminarTarea.FlatAppearance.BorderSize = 0;
            this.btnEliminarTarea.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarTarea.Font = new System.Drawing.Font("Montserrat Medium", 12F);
            this.btnEliminarTarea.ForeColor = System.Drawing.Color.White;
            this.btnEliminarTarea.Location = new System.Drawing.Point(24, 479);
            this.btnEliminarTarea.Margin = new System.Windows.Forms.Padding(2);
            this.btnEliminarTarea.Name = "btnEliminarTarea";
            this.btnEliminarTarea.Size = new System.Drawing.Size(242, 48);
            this.btnEliminarTarea.TabIndex = 21;
            this.btnEliminarTarea.Text = "ELIMINAR";
            this.btnEliminarTarea.UseVisualStyleBackColor = false;
            this.btnEliminarTarea.Click += new System.EventHandler(this.btnEliminarTarea_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnGuardar.FlatAppearance.BorderSize = 0;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Montserrat Medium", 12F);
            this.btnGuardar.ForeColor = System.Drawing.Color.White;
            this.btnGuardar.Location = new System.Drawing.Point(285, 479);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(2);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(242, 48);
            this.btnGuardar.TabIndex = 22;
            this.btnGuardar.Text = "GUARDAR";
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(49)))), ((int)(((byte)(63)))));
            this.pnlTitleBar.Controls.Add(this.lblTitleBar);
            this.pnlTitleBar.Controls.Add(this.btnCerrar);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(551, 35);
            this.pnlTitleBar.TabIndex = 24;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // lblTitleBar
            // 
            this.lblTitleBar.AutoSize = true;
            this.lblTitleBar.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblTitleBar.ForeColor = System.Drawing.Color.White;
            this.lblTitleBar.Location = new System.Drawing.Point(12, 9);
            this.lblTitleBar.Name = "lblTitleBar";
            this.lblTitleBar.Size = new System.Drawing.Size(64, 19);
            this.lblTitleBar.TabIndex = 2;
            this.lblTitleBar.Text = "Ver Tarea";
            this.lblTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lblTitleBar_MouseDown);
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(0)))), ((int)(((byte)(15)))));
            this.btnCerrar.FlatAppearance.BorderSize = 0;
            this.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.ForeColor = System.Drawing.Color.White;
            this.btnCerrar.Image = global::CapaGUI.Properties.Resources.cerrar;
            this.btnCerrar.Location = new System.Drawing.Point(496, 5);
            this.btnCerrar.Margin = new System.Windows.Forms.Padding(5);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(50, 25);
            this.btnCerrar.TabIndex = 0;
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // tmrAnimRecordatorio
            // 
            this.tmrAnimRecordatorio.Interval = 2;
            this.tmrAnimRecordatorio.Tick += new System.EventHandler(this.tmrAnimRecordatorio_Tick);
            // 
            // btnEstadoTarea
            // 
            this.btnEstadoTarea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnEstadoTarea.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnEstadoTarea.FlatAppearance.BorderSize = 4;
            this.btnEstadoTarea.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstadoTarea.Font = new System.Drawing.Font("Montserrat Medium", 12F);
            this.btnEstadoTarea.ForeColor = System.Drawing.Color.White;
            this.btnEstadoTarea.Image = global::CapaGUI.Properties.Resources.check_symbol;
            this.btnEstadoTarea.Location = new System.Drawing.Point(481, 55);
            this.btnEstadoTarea.Margin = new System.Windows.Forms.Padding(2);
            this.btnEstadoTarea.Name = "btnEstadoTarea";
            this.btnEstadoTarea.Size = new System.Drawing.Size(46, 46);
            this.btnEstadoTarea.TabIndex = 25;
            this.btnEstadoTarea.UseVisualStyleBackColor = false;
            this.btnEstadoTarea.Click += new System.EventHandler(this.btnEstadoTarea_Click);
            this.btnEstadoTarea.MouseEnter += new System.EventHandler(this.btnEstadoTarea_MouseEnter);
            // 
            // PantallaVerTarea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(551, 550);
            this.Controls.Add(this.btnEstadoTarea);
            this.Controls.Add(this.pnlTitleBar);
            this.Controls.Add(this.pnlRecordatorio);
            this.Controls.Add(this.btnRecordatorio);
            this.Controls.Add(this.btnRepetirTarea);
            this.Controls.Add(this.btnPrioridad);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlTxtNombreTarea);
            this.Controls.Add(this.btnEliminarTarea);
            this.Controls.Add(this.btnGuardar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "PantallaVerTarea";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VerTarea";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PantallaVerTarea_FormClosing);
            this.Load += new System.EventHandler(this.VerTarea_Load);
            this.pnlRecordatorio.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlTxtNombreTarea.ResumeLayout(false);
            this.pnlTxtNombreTarea.PerformLayout();
            this.pnlTitleBar.ResumeLayout(false);
            this.pnlTitleBar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlRecordatorio;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DateTimePicker dtpDiaRecordatorio;
        private System.Windows.Forms.DateTimePicker dtpHoraRecordatorio;
        private System.Windows.Forms.Button btnRecordatorio;
        private System.Windows.Forms.Button btnRepetirTarea;
        private System.Windows.Forms.Button btnPrioridad;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtDetalleTarea;
        private System.Windows.Forms.Panel pnlTxtNombreTarea;
        private System.Windows.Forms.TextBox txtNombreTarea;
        private System.Windows.Forms.Button btnEliminarTarea;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Panel pnlTitleBar;
        private System.Windows.Forms.Label lblTitleBar;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Timer tmrAnimRecordatorio;
        private System.Windows.Forms.Button btnEstadoTarea;
    }
}
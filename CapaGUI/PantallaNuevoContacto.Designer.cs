﻿namespace CapaGUI
{
    partial class PantallaNuevoContacto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNombreContacto = new System.Windows.Forms.TextBox();
            this.txtFonoContacto = new System.Windows.Forms.TextBox();
            this.btnCrearContacto = new System.Windows.Forms.Button();
            this.lblNombreContacto = new System.Windows.Forms.Label();
            this.lblTelefonoContacto = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtNombreContacto
            // 
            this.txtNombreContacto.Location = new System.Drawing.Point(44, 61);
            this.txtNombreContacto.Name = "txtNombreContacto";
            this.txtNombreContacto.Size = new System.Drawing.Size(178, 20);
            this.txtNombreContacto.TabIndex = 0;
            // 
            // txtFonoContacto
            // 
            this.txtFonoContacto.Location = new System.Drawing.Point(44, 109);
            this.txtFonoContacto.Name = "txtFonoContacto";
            this.txtFonoContacto.Size = new System.Drawing.Size(178, 20);
            this.txtFonoContacto.TabIndex = 1;
            // 
            // btnCrearContacto
            // 
            this.btnCrearContacto.Location = new System.Drawing.Point(47, 153);
            this.btnCrearContacto.Name = "btnCrearContacto";
            this.btnCrearContacto.Size = new System.Drawing.Size(175, 23);
            this.btnCrearContacto.TabIndex = 2;
            this.btnCrearContacto.Text = "Crear";
            this.btnCrearContacto.UseVisualStyleBackColor = true;
            this.btnCrearContacto.Click += new System.EventHandler(this.btnCrearContacto_Click);
            // 
            // lblNombreContacto
            // 
            this.lblNombreContacto.AutoSize = true;
            this.lblNombreContacto.Location = new System.Drawing.Point(44, 42);
            this.lblNombreContacto.Name = "lblNombreContacto";
            this.lblNombreContacto.Size = new System.Drawing.Size(44, 13);
            this.lblNombreContacto.TabIndex = 3;
            this.lblNombreContacto.Text = "Nombre";
            // 
            // lblTelefonoContacto
            // 
            this.lblTelefonoContacto.AutoSize = true;
            this.lblTelefonoContacto.Location = new System.Drawing.Point(44, 93);
            this.lblTelefonoContacto.Name = "lblTelefonoContacto";
            this.lblTelefonoContacto.Size = new System.Drawing.Size(49, 13);
            this.lblTelefonoContacto.TabIndex = 4;
            this.lblTelefonoContacto.Text = "Teléfono";
            // 
            // PantallaNuevoContacto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 242);
            this.Controls.Add(this.lblTelefonoContacto);
            this.Controls.Add(this.lblNombreContacto);
            this.Controls.Add(this.btnCrearContacto);
            this.Controls.Add(this.txtFonoContacto);
            this.Controls.Add(this.txtNombreContacto);
            this.Name = "PantallaNuevoContacto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PantallaNuevoContacto";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNombreContacto;
        private System.Windows.Forms.TextBox txtFonoContacto;
        private System.Windows.Forms.Button btnCrearContacto;
        private System.Windows.Forms.Label lblNombreContacto;
        private System.Windows.Forms.Label lblTelefonoContacto;
    }
}
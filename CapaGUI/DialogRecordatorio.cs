﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDTO;
using CapaNegocio;

namespace CapaGUI
{
    public partial class DialogRecordatorio : Form
    {
        #region VARIABLES DE INSTANCIA, PROPERTIES 

        private Tarea auxTarea;
        private PantallaInicio pInicio;

        /********** PROPERTIES ***********/
        public Tarea AuxTarea { get => auxTarea; set => auxTarea = value; }
        public PantallaInicio PInicio { get => pInicio; set => pInicio = value; }

        #endregion VARIABLES DE INSTANCIA, PROPERTIES 


        /********** INICIALIZACION ***********/
        public DialogRecordatorio(Tarea tarea, PantallaInicio pInicio)
        {
            InitializeComponent();
            this.AuxTarea = tarea;
            this.PInicio = pInicio;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DialogRecordatorio_FormClosing);
        }

        private void DialogRecordatorio_Load(object sender, EventArgs e)
        {
            Rectangle workingArea = Screen.GetWorkingArea(this);
            this.Location = new Point(workingArea.Right - (Size.Width + 20),
                                      workingArea.Bottom - (Size.Height + 20));

            lblNombreTarea.Text = this.AuxTarea.NombreTarea;
            cbxPostergarRecordatorio.Enabled = false;
            cbxPostergarRecordatorio.SelectedIndex = 0;
        }


        #region DIALOG RECORDATORIO - Botones

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (chkPostergarRecordatorio.Checked)
            {
                //Postergar recordatorio
                NegocioRecordatorio auxNegocioRecordatorio = new NegocioRecordatorio();
                Recordatorio auxRecordatorio = new Recordatorio();

                auxRecordatorio = auxNegocioRecordatorio.buscarRecordatorio(this.AuxTarea.IdTarea);

                int postergar = cbxPostergarRecordatorio.SelectedIndex;

                switch (postergar)
                {
                    case 0: //15 minutos
                        auxRecordatorio.FechaRecordatorio = DateTime.Now.AddMinutes(15);
                        break;
                    case 1: //30 minutos
                        auxRecordatorio.FechaRecordatorio = DateTime.Now.AddMinutes(30);
                        break;
                    case 2: //1 hora
                        auxRecordatorio.FechaRecordatorio = DateTime.Now.AddHours(1);
                        break;
                    case 3: //24 horas
                        auxRecordatorio.FechaRecordatorio = DateTime.Now.AddHours(24);
                        break;
                }
                auxRecordatorio.EstadoRecordatorio = false;
                auxNegocioRecordatorio.modificarRecordatorio(auxRecordatorio);
            }

            this.Dispose();
            System.GC.Collect();
            this.PInicio.cargarRecordatorios();
        }//Fin boton Aceptar Click

        private void btnVerTarea_Click(object sender, EventArgs e)
        {
            PantallaVerTarea pVerTarea = new PantallaVerTarea(this.AuxTarea, this.PInicio);
            pVerTarea.ShowDialog();
        }//Fin boton Ver Tarea Click

        #endregion DIALOG RECORDATORIO - Botones


        #region DIALOG RECORDATORIO - Eventos

        private void chkPostergarRecordatorio_CheckedChanged(object sender, EventArgs e)
        {
            cbxPostergarRecordatorio.Enabled = true;
        }

        private void DialogRecordatorio_FormClosing(object sender, FormClosingEventArgs e)
        {
            //this.PInicio.cargarTareasPendientes();
            //this.PInicio.cargarTareasTerminadas();
            //this.PInicio.cargarRecordatorios();
        }

        #endregion DIALOG RECORDATORIO - Eventos




    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using CapaDTO;
using System.IO;
using System.Text.RegularExpressions;

namespace CapaGUI
{
    public partial class PantallaInicio : Form
    {
        #region ************** VARIABLES DE INSTANCIA, PROPERTIES **************
        private List<Recordatorio> auxListaRecordatorios = new List<Recordatorio>();
        private int tabSelectedIndex;
        private bool pnlOculto;

        /********* PROPERTIES **********/
        public List<Recordatorio> AuxListaRecordatorios { get => auxListaRecordatorios; set => auxListaRecordatorios = value; }
        public int TabSelectedIndex { get => tabSelectedIndex; set => tabSelectedIndex = value; }
        public bool PnlOculto { get => pnlOculto; set => pnlOculto = value; }

        //Constantes necesarias para arrastrar desde panel (Barra de titulo)
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        // Constantes necesarias para Minimizar desde barra de tareas
        const int WS_MINIMIZEBOX = 0x20000;
        const int CS_DBLCLKS = 0x8;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style |= WS_MINIMIZEBOX;
                cp.ClassStyle |= CS_DBLCLKS;
                return cp;
            }
        } //Fin Minimizar desde barra de tareas

        #endregion ************** VARIABLES DE INSTANCIA, PROPERTIES **************


        /********* INICIALIZACION **********/
        public PantallaInicio()
        {
            InitializeComponent();
        }

        private void PantallaInicio_Load(object sender, EventArgs e)
        {
            //Se oculta panel de textbox
            PnlOculto = true;

            //Se inicializa estilo de pestaña activa
            this.tbcPestanas.SelectedIndex = 0;
            this.TabSelectedIndex = 0;
            this.pnlSelected.Location = this.btnTareas.Location;
            this.pnlSelected.Width = 10;
            this.btnTareas.ForeColor = Color.White;
            this.btnTareas.BackColor = System.Drawing.Color.FromArgb(34, 49, 63);
            this.btnTareas.Image = CapaGUI.Properties.Resources.to_do;
            
            //Cambiar estado de tareas repetibles
            NegocioTarea auxNegocioTarea = new NegocioTarea();
            List<Tarea> auxListaTareas = new List<Tarea>();
            
            auxListaTareas = auxNegocioTarea.listarTareasRepetibles();
            foreach(Tarea auxTarea in auxListaTareas)
            {
                if (auxTarea.UltimaRepeticion < DateTime.Today)
                {
                    auxTarea.EstadoTarea = false;
                    auxNegocioTarea.modificarTarea(auxTarea);
                    auxNegocioTarea.repetirTarea(auxTarea);
                }
            }

            //Carga tareas pendientes en la lista
            cargarTareasPendientes();
            
            //Cargar Recordatorios
            cargarRecordatorios();

            //CONTACTOS
            cargarContactos();

            //CONFIGURACION
            cbxSeleccionarTabla.SelectedIndex = 0;
            cbxSeleccionarTabla.Enabled = false;
        }



        #region ************** TAREAS **************

        /********* METODOS DE INSTANCIA DE FORM **********/
        public void cargarTareasPendientes()
        {
            DataTable auxDataTable = new DataTable();
            auxDataTable.Columns.Add("Estado", typeof(Boolean));
            auxDataTable.Columns.Add("Nombre", typeof(String));
            auxDataTable.Columns.Add("Id", typeof(int));
            auxDataTable.Columns.Add("Prioridad", typeof(int));
            auxDataTable.Columns.Add("Fecha Creacion", typeof(DateTime));

            NegocioTarea auxNegocio = new NegocioTarea();
            List<Tarea> auxListaTareas = new List<Tarea>();
            auxListaTareas = auxNegocio.listarTareasEstado(0);

            foreach (Tarea auxTarea in auxListaTareas)
            {
                DataRow auxDataRow = auxDataTable.NewRow();
                auxDataRow["Estado"] = auxTarea.EstadoTarea;
                auxDataRow["Nombre"] = auxTarea.NombreTarea;
                auxDataRow["Id"] = auxTarea.IdTarea;
                auxDataRow["Prioridad"] = auxTarea.PrioridadTarea;
                auxDataRow["Fecha Creacion"] = auxTarea.FechaCreacionTarea;
                auxDataTable.Rows.Add(auxDataRow);

            }

            dgTareasPendientes.DataSource = auxDataTable;

            dgTareasPendientes.Columns["Id"].Visible = false;
            dgTareasPendientes.Columns["Fecha Creacion"].Visible = false;
            //dgTareasPendientes.Columns["Prioridad"].Visible = false;
            dgTareasPendientes.Columns["Estado"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgTareasPendientes.Columns["Nombre"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgTareasPendientes.Columns["Prioridad"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            //dgTareasPendientes.Columns["Importante"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgTareasPendientes.Columns["Nombre"].ReadOnly = true;

            this.dgTareasPendientes.ClearSelection();

            //dgTareasPendientes.Sort(dgTareasPendientes.Columns["Fecha Creacion"], ListSortDirection.Descending);

            /*
            //foreach (DataGridViewRow auxFila in dgTareasPendientes.Rows)
            //{
            //    if (auxFila.Cells["Prioridad"].Value.ToString() == "1")
            //    {
            //        Image image = Properties.Resources.star;
            //        auxFila.Cells["Importante"].Value = image;
            //    }
            //    else
            //    {
            //        Image image = Properties.Resources.grey_star;
            //        auxFila.Cells["Importante"].Value = image;           //    }
            //}
            */
        } //Fin Carga Tareas Pendientes

        public void cargarRecordatorios()
        {
            this.tmrRecordatorios.Stop();
            this.Refresh();
            NegocioRecordatorio auxNegocioRecordatorio = new NegocioRecordatorio();
            this.AuxListaRecordatorios = auxNegocioRecordatorio.listarRecordatoriosHoy();
            this.tmrRecordatorios.Start();
        }//Fin Carga Recordatorios

        #region TAREAS - Botones

        private void btnEliminarTareaPendiente_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea eliminar la tarea?", "Confirmación", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                NegocioTarea auxNegocio = new NegocioTarea();

                int indiceFila = dgTareasPendientes.SelectedCells[0].RowIndex;
                DataGridViewRow filaSeleccionada = dgTareasPendientes.Rows[indiceFila];

                auxNegocio.eliminarTarea(int.Parse(filaSeleccionada.Cells["Id"].Value.ToString()));
                cargarTareasPendientes();
            }
        } //Fin Eliminar Tarea Pendiente Click

        private void btnVerTareaPendiente_Click(object sender, EventArgs e)
        {
            if (dgTareasPendientes.RowCount > 0 && dgTareasPendientes.SelectedCells.Count > 0)
            {
                NegocioTarea auxNegocio = new NegocioTarea();
                Tarea auxTarea = new Tarea();
                
                int indiceFila = dgTareasPendientes.SelectedCells[0].RowIndex;
                DataGridViewRow filaSeleccionada = dgTareasPendientes.Rows[indiceFila];

                auxTarea = auxNegocio.buscarTarea(int.Parse(filaSeleccionada.Cells["Id"].Value.ToString()));

                PantallaVerTarea pVerTarea = new PantallaVerTarea(auxTarea, this);
                pVerTarea.ShowDialog();
            }
        } //Fin Ver Tarea Pendiente Click

        private void btnNuevaTarea_Click(object sender, EventArgs e)
        {
            if (this.btnNuevaTarea.Text == "NUEVA")
            {

                this.txtInputText.Text = String.Empty;
                this.btnBuscarTarea.Text = "CANCELAR";
                this.btnNuevaTarea.Text = "CREAR";
                this.txtInputText.Focus();
                this.tmrTextBox.Start();
            }
            else if (this.btnNuevaTarea.Text == "CANCELAR")
            {

                this.btnNuevaTarea.Text = "NUEVA";
                this.tmrTextBox.Start();
            }
            else
            {

                this.btnNuevaTarea.Text = "NUEVA";
                this.btnBuscarTarea.Text = "BUSCAR";
                PantallaNuevaTarea pNuevaTarea = new PantallaNuevaTarea(this, this.txtInputText.Text);
                pNuevaTarea.ShowDialog();
                this.tmrTextBox.Start();
            }

        } //Fin Boton Nueva Tarea

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (this.btnBuscarTarea.Text == "BUSCAR")
            {
                if (this.PnlOculto)
                {
                    this.tmrTextBox.Start();
                    this.txtInputText.Text = String.Empty;
                    this.btnNuevaTarea.Text = "CANCELAR";
                    this.txtInputText.Focus();
                }
                else
                {
                    this.tmrTextBox.Start();
                    this.btnNuevaTarea.Text = "NUEVA";
                    this.btnBuscarTarea.Text = "BUSCAR";
                    PantallaBuscarTarea pBuscarTarea = new PantallaBuscarTarea(this, this.txtInputText.Text);
                    pBuscarTarea.ShowDialog();
                }
            }
            else
            {
                this.tmrTextBox.Start();
                this.btnBuscarTarea.Text = "BUSCAR";
                this.btnNuevaTarea.Text = "NUEVA";
            }
        }//Fin Boton Buscar

        private void dgTareasPendientes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnVerTareaPendiente_Click(sender, e);
        }//Fin Cell DoubleClick en Item Lista Tareas

        #endregion TAREAS - Botones

       
        #region TAREAS - GUI

        /*********** ANIMACION TEXTBOX ***********/
        private void tmrTextBox_Tick(object sender, EventArgs e)
        {
            if (PnlOculto)
            {
                this.pnlTextBoxTareas.Height = this.pnlTextBoxTareas.Height + 4;
                this.dgTareasPendientes.Height = this.dgTareasPendientes.Height - 5;
                this.dgTareasPendientes.Location = new Point(this.dgTareasPendientes.Location.X, this.dgTareasPendientes.Location.Y + 5);

                if (this.pnlTextBoxTareas.Height >= 44)
                {
                    this.dgTareasPendientes.Height = this.dgTareasPendientes.Height - 4;
                    this.dgTareasPendientes.Location = new Point(this.dgTareasPendientes.Location.X, this.dgTareasPendientes.Location.Y + 4);
                    this.tmrTextBox.Stop();
                    this.Refresh();
                    this.PnlOculto = false;
                }
            }
            else
            {
                this.pnlTextBoxTareas.Height = this.pnlTextBoxTareas.Height - 4;
                this.dgTareasPendientes.Height = this.dgTareasPendientes.Height + 5;
                this.dgTareasPendientes.Location = new Point(this.dgTareasPendientes.Location.X, this.dgTareasPendientes.Location.Y - 5);
                if (this.pnlTextBoxTareas.Height <= 0)
                {
                    this.dgTareasPendientes.Height = this.dgTareasPendientes.Height + 4;
                    this.dgTareasPendientes.Location = new Point(this.dgTareasPendientes.Location.X, this.dgTareasPendientes.Location.Y - 4);
                    this.tmrTextBox.Stop();
                    this.Refresh();
                    this.PnlOculto = true;
                }
            }

        } //Fin timer animacion textbox

        private void txtInputText_KeyDown(object sender, KeyEventArgs e)
        {
            if (!PnlOculto)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (this.btnNuevaTarea.Text == "CREAR")
                    {
                        e.SuppressKeyPress = true;
                        tmrBtnNuevaTarea.Enabled = true;
                    }
                    else
                    {
                        e.SuppressKeyPress = true;
                        tmrBtnBuscar.Enabled = true;
                    }

                }

                if (e.KeyCode == Keys.Escape)
                {
                    if (this.btnNuevaTarea.Text == "CREAR")
                    {
                        e.SuppressKeyPress = true;
                        tmrBtnBuscar.Enabled = true;
                    }
                    else
                    {
                        e.SuppressKeyPress = true;
                        tmrBtnNuevaTarea.Enabled = true;

                    }
                    this.picLogo.Focus();
                }
            }


        } //Fin hotkeys textbox (Esc, Enter)
        #endregion TAREAS - GUI


        #region TAREAS - Eventos On Change

        private void dgTareasPendientes_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex > -1)
            {
                NegocioTarea auxNegocio = new NegocioTarea();
                Tarea auxTarea = new Tarea();

                int indiceFila = dgTareasPendientes.SelectedCells[0].RowIndex;

                DataGridViewRow filaSeleccionada = dgTareasPendientes.Rows[indiceFila];

                //Cambiar estado de tarea
                auxTarea = auxNegocio.buscarTarea(int.Parse(filaSeleccionada.Cells["Id"].Value.ToString()));
                auxTarea.EstadoTarea = true;
                auxNegocio.modificarTarea(auxTarea);

                //Recargar Tareas
                cargarTareasPendientes();

                NegocioRecordatorio auxNegocioRecordatorio = new NegocioRecordatorio();
                Recordatorio auxRecordatorio = new Recordatorio();

                //Eliminar Recordatorio de tarea
                auxRecordatorio = auxNegocioRecordatorio.buscarRecordatorio(auxTarea.IdTarea);
                auxNegocioRecordatorio.eliminarRecordatorio(auxRecordatorio.IdRecordatorio);

                //Recargar recordatorios
                cargarRecordatorios();
            }
        }//Fin Cambiar estado de Tarea

        private void dgTareasPendientes_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            //Evento para mejorar el funcionamiento de CellValueChanged
            if (e.ColumnIndex == 0 && e.RowIndex != -1)
            {
                dgTareasPendientes.EndEdit();
            }
        }

        private void tmrRecordatorios_Tick(object sender, EventArgs e)
        {
            foreach (Recordatorio auxRecordatorio in this.AuxListaRecordatorios)
            {
                TimeSpan horaRecordatorio = auxRecordatorio.FechaRecordatorio.TimeOfDay;
                TimeSpan horaActual = DateTime.Now.TimeOfDay;

                if (horaRecordatorio <= horaActual && auxRecordatorio.FlagRecordatorio == true)
                {
                    NegocioRecordatorio auxNegocioRecordatorio = new NegocioRecordatorio();

                    auxRecordatorio.EstadoRecordatorio = true;
                    auxRecordatorio.FlagRecordatorio = false;
                    auxNegocioRecordatorio.modificarRecordatorio(auxRecordatorio);

                    NegocioTarea auxNegocioTarea = new NegocioTarea();
                    Tarea auxTarea = new Tarea();

                    auxTarea = auxNegocioTarea.buscarTarea(auxRecordatorio.IdTarea);

                    DialogRecordatorio dRecordatorio = new DialogRecordatorio(auxTarea, this);
                    dRecordatorio.Show();
                }
            }
        }//Fin timer recordatorios

        // Timers para eliminar sonido de windows al apretar enter y esc
        private void tmrBtnNuevaTarea_Tick(object sender, EventArgs e)
        {
            tmrBtnNuevaTarea.Enabled = false;
            btnNuevaTarea_Click(sender, e);
        }

        private void tmrBtnBuscar_Tick(object sender, EventArgs e)
        {
            tmrBtnBuscar.Enabled = false;
            btnBuscar_Click(sender, e);
        }

        #endregion TAREAS - Eventos On Change


        #endregion ************** TAREAS **************


        #region ************** FACTURACION **************
        #endregion ************** FACTURACION **************


        #region ************** MAIL FACTURAS **************
        #endregion ************** MAIL FACTURAS **************


        #region ************** CONTACTOS **************

        /**************** METODOS DE INSTANCIA DE FORM ****************/

        public void cargarContactos()
        {
            DataTable auxDataTable = new DataTable();           
            auxDataTable.Columns.Add("Nombre", typeof(String));
            auxDataTable.Columns.Add("Telefono", typeof(String));
            auxDataTable.Columns.Add("Id", typeof(int));

            NegocioContacto auxNegocioContacto = new NegocioContacto();
            List<Contacto> auxListaContactos = new List<Contacto>();

            auxListaContactos = auxNegocioContacto.listarContactos();

            foreach (Contacto auxContacto in auxListaContactos)
            {
                DataRow auxDataRow = auxDataTable.NewRow();
                auxDataRow["Nombre"] = auxContacto.NombreContacto;
                auxDataRow["Telefono"] = auxContacto.TelefonoContacto;
                auxDataRow["Id"] = auxContacto.IdContacto;
                auxDataTable.Rows.Add(auxDataRow);

            }

            dgContactos.DataSource = auxDataTable;

            dgContactos.Columns["Id"].Visible = false;
            dgContactos.Columns["Nombre"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgContactos.Columns["Telefono"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgContactos.Columns["Nombre"].ReadOnly = true;
            dgContactos.Columns["Telefono"].ReadOnly = true;

            this.dgContactos.ClearSelection();

        } //Fin Cargar Contactos

        #region CONTACTOS - Botones

        private void btnAgregarContacto_Click(object sender, EventArgs e)
        {
            PantallaNuevoContacto pNuevoContacto = new PantallaNuevoContacto(this);
            pNuevoContacto.ShowDialog();

        } //Fin Boton Agregar Contacto

        private void btnModificarContacto_Click(object sender, EventArgs e)
        {
            if (dgContactos.RowCount > 0 && dgContactos.SelectedCells.Count > 0)
            {
                int indiceFila = dgContactos.SelectedCells[0].RowIndex;
                DataGridViewRow filaSeleccionada = dgContactos.Rows[indiceFila];

                NegocioContacto auxNegocioContacto = new NegocioContacto();
                Contacto auxContacto = new Contacto();

                auxContacto = auxNegocioContacto.buscarContacto(int.Parse(filaSeleccionada.Cells["Id"].Value.ToString()));

                PantallaModificarContacto pModificarContacto = new PantallaModificarContacto(this, auxContacto);
                pModificarContacto.ShowDialog();
            }

        } //Fin Boton Modificar Contacto

        private void btnEliminarContacto_Click(object sender, EventArgs e)
        {
            if (dgContactos.RowCount > 0 && dgContactos.SelectedCells.Count > 0)
            {
                DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea eliminar el Contacto?", "Confirmación", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    NegocioContacto auxNegocioContacto = new NegocioContacto();

                    int indiceFila = dgContactos.SelectedCells[0].RowIndex;
                    DataGridViewRow filaSeleccionada = dgContactos.Rows[indiceFila];

                    auxNegocioContacto.eliminarContacto(int.Parse(filaSeleccionada.Cells["Id"].Value.ToString()));
                    cargarContactos();
                }
            }

        } //Fin Boton ELiminar Contacto

        #endregion CONTACTOS - Botones
        
        #region CONTACTOS - Eventos On Change

        private void dgContactos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnModificarContacto_Click(sender, e);
        }

        #endregion CONTACTO - Eventos On Change

        #endregion ************** CONTACTOS **************


        #region ************** CONFIGURACION **************

        private void btnProcesar_Click(object sender, EventArgs e)
        {
            if (this.rbImportar.Checked)
            {
                //IMPORTAR DATOS
                OpenFileDialog choofdlog = new OpenFileDialog();
                choofdlog.Filter = "All Files (*.*)|*.*";
                choofdlog.FilterIndex = 1;
                choofdlog.Multiselect = false;

                if (choofdlog.ShowDialog() == DialogResult.OK)
                {
                    string rutaArchivoImportarDatos = choofdlog.FileName;

                    try
                    {
                        StreamReader reader = new StreamReader(File.OpenRead(rutaArchivoImportarDatos), Encoding.GetEncoding("iso-8859-1"));
                        List<String> colNombreTarea = new List<String>();
                        List<String> colDetalleTarea = new List<String>();
                        List<String> colFechaCreacionTarea = new List<String>();
                        List<String> colFechaModificacionTarea = new List<String>();
                        List<String> colPrioridadTarea = new List<String>();
                        List<String> colRepetible = new List<String>();
                        List<String> colEstadoTarea = new List<String>();

                        NegocioTarea auxNegocioTarea = new NegocioTarea();
                        Tarea auxTarea = new Tarea();
                        
                        while (!reader.EndOfStream)
                        {
                            string line = reader.ReadLine();
                            if (!String.IsNullOrWhiteSpace(line))
                            {
                                string[] values = line.Split(';');

                                auxTarea.NombreTarea = values[0];
                                auxTarea.DetalleTarea = values[1].Replace(@"\n", System.Environment.NewLine);
                                auxTarea.FechaCreacionTarea = Convert.ToDateTime(values[2]);
                                auxTarea.FechaModificacionTarea = Convert.ToDateTime(values[3]);
                                auxTarea.PrioridadTarea = Convert.ToBoolean((values[4]=="1") ? true : false);
                                auxTarea.Repetible = Convert.ToBoolean((values[5] == "1") ? true : false);
                                auxTarea.EstadoTarea = Convert.ToBoolean((values[6] == "1") ? true : false);

                                auxNegocioTarea.insertarTarea(auxTarea);

                            }
                        }

                        MessageBox.Show("Se han importado los datos desde " + rutaArchivoImportarDatos + ".");
                        cargarTareasPendientes();

                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show("No se pudo abrir el archivo. Asegúrese de que el archivo no se encuentre abierto." + "\n\nError: " + ex.Message, "Error");
                    }
                    
                }

            }
            else
            {
                //EXPORTAR DATOS
                if (this.cbxSeleccionarTabla.SelectedIndex > 0)
                {
                    
                    FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
                    if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                    {
                        string nombreTabla = this.cbxSeleccionarTabla.Text;
                        string rutaArchivoDestino = folderBrowserDialog.SelectedPath;
                        rutaArchivoDestino = rutaArchivoDestino + @"\" + nombreTabla + "_" + DateTime.Now.ToString("dd-MM-yyyy") + ".csv";

                        //Creacion del archivo
                        switch (nombreTabla)
                        {
                            case "Tareas":
                                try
                                {
                                    using (StreamWriter sw = new StreamWriter(File.Open(rutaArchivoDestino, FileMode.Create), Encoding.GetEncoding("iso-8859-1")))
                                    {
                                        NegocioTarea auxNegocioTarea = new NegocioTarea();
                                        List<Tarea> auxListaTareas = new List<Tarea>();

                                        auxListaTareas = auxNegocioTarea.listarTareas();

                                        foreach (Tarea tarea in auxListaTareas)
                                        {
                                            sw.WriteLine(tarea.NombreTarea.Replace(";", "")
                                                + ";" + Regex.Replace(tarea.DetalleTarea.Replace(";", ""), @"\r\n?|\n", @"\n")
                                                + ";" + tarea.FechaCreacionTarea
                                                + ";" + tarea.FechaModificacionTarea
                                                + ";" + ((tarea.PrioridadTarea) ? 1 : 0)
                                                + ";" + ((tarea.Repetible) ? 1 : 0)
                                                + ";" + ((tarea.EstadoTarea) ? 1 : 0));
                                        }
                                    }

                                    

                                    MessageBox.Show("Se han respaldado las Tareas en " + rutaArchivoDestino, "Archivo Generado");
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("No se pudo crear el archivo." + "\n\nError: " + ex.Message, "Error");
                                }

                                break;
                            case "Contactos":
                                MessageBox.Show("Funcionalidad no implementada.");
                                break;
                        }

                        
                    }
                }
                else
                {
                    MessageBox.Show("Por favor, seleccione la tabla que desea exportar.");
                }
                
            }
        } //Fin Boton Procesar

        #region CONFIGURACION - EVENTOS ON CHANGE

        private void rbExportar_CheckedChanged(object sender, EventArgs e)
        {
            this.cbxSeleccionarTabla.Enabled = true;
        }

        private void rbImportar_CheckedChanged(object sender, EventArgs e)
        {
            this.cbxSeleccionarTabla.Enabled = false;
        }

        #endregion CONFIGURACION - EVENTOS ON CHANGE


        #endregion ************** CONFIGURACION **************


        #region ************** GUI **************

        private void resetColores(int selected)
        {
            if (selected != 0)
            {
                this.btnTareas.ForeColor = System.Drawing.Color.FromArgb(62, 137, 146);
                this.btnTareas.BackColor = System.Drawing.Color.FromArgb(44, 62, 80);
                this.btnTareas.Image = CapaGUI.Properties.Resources.to_do_color;
            }

            if (selected != 1)
            {
                this.btnFacturacion.ForeColor = System.Drawing.Color.FromArgb(62, 137, 146);
                this.btnFacturacion.BackColor = System.Drawing.Color.FromArgb(44, 62, 80);
                this.btnFacturacion.Image = CapaGUI.Properties.Resources.factura_color;
            }

            if (selected != 2)
            {
                this.btnMailFacturas.ForeColor = System.Drawing.Color.FromArgb(62, 137, 146);
                this.btnMailFacturas.BackColor = System.Drawing.Color.FromArgb(44, 62, 80);
                this.btnMailFacturas.Image = CapaGUI.Properties.Resources.mail_color;
            }

            if (selected != 3)
            {
                this.btnContactos.ForeColor = System.Drawing.Color.FromArgb(62, 137, 146);
                this.btnContactos.BackColor = System.Drawing.Color.FromArgb(44, 62, 80);
                this.btnContactos.Image = CapaGUI.Properties.Resources.contacts_color;
            }

            if (selected != 4)
            {
                this.btnConfiguracion.ForeColor = System.Drawing.Color.FromArgb(62, 137, 146);
                this.btnConfiguracion.BackColor = System.Drawing.Color.FromArgb(44, 62, 80);
                this.btnConfiguracion.Image = CapaGUI.Properties.Resources.settings_color;
            }

            this.TabSelectedIndex = selected;
        }

        /*********** BOTONES PESTAÑAS ***********/
        private void btnTareas_Click(object sender, EventArgs e)
        {
            this.tbcPestanas.SelectedIndex = 0;
            this.pnlSelected.Location = this.btnTareas.Location;
            this.pnlSelected.Width = 10;
            this.btnTareas.ForeColor = Color.White;
            this.btnTareas.BackColor = System.Drawing.Color.FromArgb(34, 49, 63);
            this.btnTareas.Image = CapaGUI.Properties.Resources.to_do;

            this.pnlSelected.Location = this.btnTareas.Location;
            this.pnlSelected.Width = 10;

            resetColores(0);

        }

        private void btnFacturacion_Click(object sender, EventArgs e)
        {
            this.tbcPestanas.SelectedIndex = 1;
            this.pnlSelected.Location = this.btnFacturacion.Location;
            this.pnlSelected.Width = 10;
            this.btnFacturacion.ForeColor = Color.White;
            this.btnFacturacion.BackColor = System.Drawing.Color.FromArgb(34, 49, 63);
            this.btnFacturacion.Image = CapaGUI.Properties.Resources.factura;

            this.pnlSelected.Location = this.btnFacturacion.Location;
            this.pnlSelected.Width = 10;

            resetColores(1);
        }

        private void btnMailFacturas_Click(object sender, EventArgs e)
        {
            this.tbcPestanas.SelectedIndex = 2;
            this.pnlSelected.Location = this.btnMailFacturas.Location;
            this.pnlSelected.Width = 10;
            this.btnMailFacturas.ForeColor = Color.White;
            this.btnMailFacturas.BackColor = System.Drawing.Color.FromArgb(34, 49, 63);
            this.btnMailFacturas.Image = CapaGUI.Properties.Resources.mail;

            this.pnlSelected.Location = this.btnMailFacturas.Location;
            this.pnlSelected.Width = 10;

            resetColores(2);
        }

        private void btnContactos_Click(object sender, EventArgs e)
        {
            this.tbcPestanas.SelectedIndex = 3;
            this.pnlSelected.Location = this.btnContactos.Location;
            this.pnlSelected.Width = 10;
            this.btnContactos.ForeColor = Color.White;
            this.btnContactos.BackColor = System.Drawing.Color.FromArgb(34, 49, 63);
            this.btnContactos.Image = CapaGUI.Properties.Resources.contacts;

            this.pnlSelected.Location = this.btnContactos.Location;
            this.pnlSelected.Width = 10;

            resetColores(3);
        }

        private void btnConfiguracion_Click(object sender, EventArgs e)
        {
            this.tbcPestanas.SelectedIndex = 4;
            this.pnlSelected.Location = this.btnConfiguracion.Location;
            this.pnlSelected.Width = 10;
            this.btnConfiguracion.ForeColor = Color.White;
            this.btnConfiguracion.BackColor = System.Drawing.Color.FromArgb(34, 49, 63);
            this.btnConfiguracion.Image = CapaGUI.Properties.Resources.settings;

            this.pnlSelected.Location = this.btnConfiguracion.Location;
            this.pnlSelected.Width = 10;

            resetColores(4);
        }


        /*********** MOUSE ENTER/LEAVE ***********/
        private void btnTareas_MouseEnter(object sender, EventArgs e)
        {
            this.btnTareas.ForeColor = Color.White;
            this.btnTareas.Image = CapaGUI.Properties.Resources.to_do;
        }

        private void btnTareas_MouseLeave(object sender, EventArgs e)
        {
            if (TabSelectedIndex != 0)
            {
                this.btnTareas.ForeColor = System.Drawing.Color.FromArgb(62, 137, 146);
                this.btnTareas.Image = CapaGUI.Properties.Resources.to_do_color;
            }
        }

        private void btnFacturacion_MouseEnter(object sender, EventArgs e)
        {
            this.btnFacturacion.ForeColor = Color.White;
            this.btnFacturacion.Image = CapaGUI.Properties.Resources.factura;
        }

        private void btnFacturacion_MouseLeave(object sender, EventArgs e)
        {
            if (TabSelectedIndex != 1)
            {
                this.btnFacturacion.ForeColor = System.Drawing.Color.FromArgb(62, 137, 146);
                this.btnFacturacion.Image = CapaGUI.Properties.Resources.factura_color;
            }
        }

        private void btnMailFacturas_MouseEnter(object sender, EventArgs e)
        {
            this.btnMailFacturas.ForeColor = Color.White;
            this.btnMailFacturas.Image = CapaGUI.Properties.Resources.mail;
        }

        private void btnMailFacturas_MouseLeave(object sender, EventArgs e)
        {
            if (TabSelectedIndex != 2)
            {
                this.btnMailFacturas.ForeColor = System.Drawing.Color.FromArgb(62, 137, 146);
                this.btnMailFacturas.Image = CapaGUI.Properties.Resources.mail_color;
            }
        }

        private void btnContactos_MouseEnter(object sender, EventArgs e)
        {
            this.btnContactos.ForeColor = Color.White;
            this.btnContactos.Image = CapaGUI.Properties.Resources.contacts;
        }

        private void btnContactos_MouseLeave(object sender, EventArgs e)
        {
            if (TabSelectedIndex != 3)
            {
                this.btnContactos.ForeColor = System.Drawing.Color.FromArgb(62, 137, 146);
                this.btnContactos.Image = CapaGUI.Properties.Resources.contacts_color;
            }

        }

        private void btnConfiguracion_MouseEnter(object sender, EventArgs e)
        {
            this.btnConfiguracion.ForeColor = Color.White;
            this.btnConfiguracion.Image = CapaGUI.Properties.Resources.settings;
        }

        private void btnConfiguracion_MouseLeave(object sender, EventArgs e)
        {
            if (TabSelectedIndex != 4)
            {
                this.btnConfiguracion.ForeColor = System.Drawing.Color.FromArgb(62, 137, 146);
                this.btnConfiguracion.Image = CapaGUI.Properties.Resources.settings_color;
            }

        }


        /*********** TITLEBAR ***********/
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void lblTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }








        #endregion ************** GUI **************


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using CapaDTO;

namespace CapaGUI
{
    public partial class PantallaNuevoContacto : Form
    {
        private PantallaInicio pInicio;

        public PantallaInicio PInicio { get => pInicio; set => pInicio = value; }

        public PantallaNuevoContacto(PantallaInicio pInicio)
        {
            this.PInicio = pInicio;
            InitializeComponent();
        }


        private void btnCrearContacto_Click(object sender, EventArgs e)
        {
            NegocioContacto auxNegocioContacto = new NegocioContacto();
            Contacto auxContacto = new Contacto();

            auxContacto.NombreContacto = txtNombreContacto.Text;
            auxContacto.TelefonoContacto = txtFonoContacto.Text;

            auxNegocioContacto.insertarContacto(auxContacto);
            
            System.GC.Collect();
            this.Dispose();
            this.PInicio.cargarContactos();
        }
    }
}

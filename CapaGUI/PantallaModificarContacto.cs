﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaDTO;
using CapaNegocio;

namespace CapaGUI
{
    public partial class PantallaModificarContacto : Form
    {
        private PantallaInicio pInicio;
        private Contacto auxContacto;

        public PantallaInicio PInicio { get => pInicio; set => pInicio = value; }
        public Contacto AuxContacto { get => auxContacto; set => auxContacto = value; }

        public PantallaModificarContacto(PantallaInicio pInicio, Contacto auxContacto)
        {
            InitializeComponent();
            this.PInicio = pInicio;
            this.AuxContacto = auxContacto;
        }

        private void PantallaModificarContacto_Load(object sender, EventArgs e)
        {
            this.txtNombreContacto.Text = AuxContacto.NombreContacto;
            this.txtTelefonoContacto.Text = AuxContacto.TelefonoContacto;
        }

        private void btnGuardarContacto_Click(object sender, EventArgs e)
        {
            NegocioContacto auxNegocioContacto = new NegocioContacto();
            Contacto auxContacto = new Contacto();

            auxContacto.IdContacto = this.AuxContacto.IdContacto;
            auxContacto.NombreContacto = this.txtNombreContacto.Text;
            auxContacto.TelefonoContacto = this.txtTelefonoContacto.Text;

            auxNegocioContacto.modificarContacto(auxContacto);

            this.Dispose();
            System.GC.Collect();
            this.PInicio.cargarContactos();
        }

        private void btnEliminarContacto_Click(object sender, EventArgs e)
        {
            //Eliminar Contacto
            DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea eliminar el Contacto?", "Confirmación", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                NegocioContacto auxNegocioContacto = new NegocioContacto();
                auxNegocioContacto.eliminarContacto(this.AuxContacto.IdContacto);

                this.Dispose();
                System.GC.Collect();
                PInicio.cargarContactos();
            }
        }
        
    }
}

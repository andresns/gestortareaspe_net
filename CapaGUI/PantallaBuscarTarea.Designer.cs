﻿namespace CapaGUI
{
    partial class PantallaBuscarTarea
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PantallaBuscarTarea));
            this.dgResultadoBusqueda = new System.Windows.Forms.DataGridView();
            this.txtTextoBusqueda = new System.Windows.Forms.TextBox();
            this.btnVerTarea = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.chkTerminadas = new System.Windows.Forms.CheckBox();
            this.chkPendientes = new System.Windows.Forms.CheckBox();
            this.cbxFechaCreacion = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgResultadoBusqueda)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgResultadoBusqueda
            // 
            this.dgResultadoBusqueda.AllowUserToAddRows = false;
            this.dgResultadoBusqueda.AllowUserToDeleteRows = false;
            this.dgResultadoBusqueda.AllowUserToResizeColumns = false;
            this.dgResultadoBusqueda.AllowUserToResizeRows = false;
            this.dgResultadoBusqueda.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgResultadoBusqueda.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgResultadoBusqueda.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgResultadoBusqueda.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgResultadoBusqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgResultadoBusqueda.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgResultadoBusqueda.GridColor = System.Drawing.SystemColors.Window;
            this.dgResultadoBusqueda.Location = new System.Drawing.Point(21, 102);
            this.dgResultadoBusqueda.Margin = new System.Windows.Forms.Padding(2);
            this.dgResultadoBusqueda.MultiSelect = false;
            this.dgResultadoBusqueda.Name = "dgResultadoBusqueda";
            this.dgResultadoBusqueda.RowHeadersVisible = false;
            this.dgResultadoBusqueda.RowTemplate.Height = 28;
            this.dgResultadoBusqueda.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgResultadoBusqueda.Size = new System.Drawing.Size(387, 244);
            this.dgResultadoBusqueda.TabIndex = 1;
            this.dgResultadoBusqueda.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgResultadoBusqueda_CellDoubleClick);
            // 
            // txtTextoBusqueda
            // 
            this.txtTextoBusqueda.Location = new System.Drawing.Point(12, 25);
            this.txtTextoBusqueda.Margin = new System.Windows.Forms.Padding(2);
            this.txtTextoBusqueda.Name = "txtTextoBusqueda";
            this.txtTextoBusqueda.Size = new System.Drawing.Size(286, 20);
            this.txtTextoBusqueda.TabIndex = 0;
            this.txtTextoBusqueda.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTextoBusqueda_KeyDown);
            // 
            // btnVerTarea
            // 
            this.btnVerTarea.Location = new System.Drawing.Point(47, 357);
            this.btnVerTarea.Margin = new System.Windows.Forms.Padding(2);
            this.btnVerTarea.Name = "btnVerTarea";
            this.btnVerTarea.Size = new System.Drawing.Size(133, 21);
            this.btnVerTarea.TabIndex = 2;
            this.btnVerTarea.Text = "Ver";
            this.btnVerTarea.UseVisualStyleBackColor = true;
            this.btnVerTarea.Click += new System.EventHandler(this.btnVerTarea_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Location = new System.Drawing.Point(247, 357);
            this.btnCerrar.Margin = new System.Windows.Forms.Padding(2);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(133, 21);
            this.btnCerrar.TabIndex = 3;
            this.btnCerrar.Text = "Cerrar";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnBuscar);
            this.groupBox1.Controls.Add(this.chkTerminadas);
            this.groupBox1.Controls.Add(this.chkPendientes);
            this.groupBox1.Controls.Add(this.cbxFechaCreacion);
            this.groupBox1.Controls.Add(this.txtTextoBusqueda);
            this.groupBox1.Location = new System.Drawing.Point(21, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(387, 84);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtro de Búsqueda";
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(303, 23);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 4;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // chkTerminadas
            // 
            this.chkTerminadas.AutoSize = true;
            this.chkTerminadas.Location = new System.Drawing.Point(297, 54);
            this.chkTerminadas.Margin = new System.Windows.Forms.Padding(2);
            this.chkTerminadas.Name = "chkTerminadas";
            this.chkTerminadas.Size = new System.Drawing.Size(81, 17);
            this.chkTerminadas.TabIndex = 3;
            this.chkTerminadas.Text = "Terminadas";
            this.chkTerminadas.UseVisualStyleBackColor = true;
            this.chkTerminadas.CheckedChanged += new System.EventHandler(this.chkTerminadas_CheckedChanged);
            // 
            // chkPendientes
            // 
            this.chkPendientes.AutoSize = true;
            this.chkPendientes.Location = new System.Drawing.Point(207, 54);
            this.chkPendientes.Margin = new System.Windows.Forms.Padding(2);
            this.chkPendientes.Name = "chkPendientes";
            this.chkPendientes.Size = new System.Drawing.Size(79, 17);
            this.chkPendientes.TabIndex = 2;
            this.chkPendientes.Text = "Pendientes";
            this.chkPendientes.UseVisualStyleBackColor = true;
            this.chkPendientes.CheckedChanged += new System.EventHandler(this.chkPendientes_CheckedChanged);
            // 
            // cbxFechaCreacion
            // 
            this.cbxFechaCreacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxFechaCreacion.FormattingEnabled = true;
            this.cbxFechaCreacion.Items.AddRange(new object[] {
            "Cualquier fecha",
            "Hoy",
            "Últimos 7 días",
            "Últimos 30 días"});
            this.cbxFechaCreacion.Location = new System.Drawing.Point(12, 53);
            this.cbxFechaCreacion.Margin = new System.Windows.Forms.Padding(2);
            this.cbxFechaCreacion.Name = "cbxFechaCreacion";
            this.cbxFechaCreacion.Size = new System.Drawing.Size(180, 21);
            this.cbxFechaCreacion.TabIndex = 1;
            this.cbxFechaCreacion.Tag = "";
            this.cbxFechaCreacion.SelectedIndexChanged += new System.EventHandler(this.cbxFechaCreacion_SelectedIndexChanged);
            // 
            // PantallaBuscarTarea
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 397);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.btnVerTarea);
            this.Controls.Add(this.dgResultadoBusqueda);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "PantallaBuscarTarea";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PantallaBuscarTarea";
            this.Load += new System.EventHandler(this.PantallaBuscarTarea_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgResultadoBusqueda)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgResultadoBusqueda;
        private System.Windows.Forms.TextBox txtTextoBusqueda;
        private System.Windows.Forms.Button btnVerTarea;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbxFechaCreacion;
        private System.Windows.Forms.CheckBox chkTerminadas;
        private System.Windows.Forms.CheckBox chkPendientes;
        private System.Windows.Forms.Button btnBuscar;
    }
}
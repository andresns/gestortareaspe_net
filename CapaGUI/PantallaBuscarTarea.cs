﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using CapaDTO;

namespace CapaGUI
{
    public partial class PantallaBuscarTarea : Form
    {
        private PantallaInicio pInicio;

        public PantallaInicio PInicio { get => pInicio; set => pInicio = value; }

        /********** INICIALIZACION ***********/
        public PantallaBuscarTarea(PantallaInicio pInicio, string textoBusqueda)
        {
            InitializeComponent();
            this.pInicio = pInicio;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PantallaBuscarTarea_FormClosing);
            txtTextoBusqueda.Text = textoBusqueda;
        }

        private void PantallaBuscarTarea_Load(object sender, EventArgs e)
        {
            cbxFechaCreacion.SelectedIndex = 0;
            chkPendientes.Checked = true;
            chkTerminadas.Checked = true;
            txtTextoBusqueda.Select();
            cargarResultadosBusqueda();
        }


        /********** METODOS CUSTOM ***********/
        public void cargarResultadosBusqueda()
        {
            //Filtros de busqueda
            string textoBusqueda = this.txtTextoBusqueda.Text.Trim();
            int fechaBusqueda = this.cbxFechaCreacion.SelectedIndex;
            int estadoBusqueda = 2;

            if (this.chkPendientes.Checked == true && this.chkTerminadas.Checked == false)
            {
                estadoBusqueda = 0;
            }

            if (this.chkPendientes.Checked == false && this.chkTerminadas.Checked == true)
            {
                estadoBusqueda = 1;
            }
            DataTable auxDataTable = new DataTable();
            auxDataTable.Columns.Add("Estado", typeof(Boolean));
            auxDataTable.Columns.Add("Nombre", typeof(String));
            auxDataTable.Columns.Add("Id", typeof(int));
            auxDataTable.Columns.Add("Fecha Creacion", typeof(DateTime));

            NegocioTarea auxNegocio = new NegocioTarea();
            List<Tarea> auxListaTareas = new List<Tarea>();

            auxListaTareas = auxNegocio.listarTareasFiltro(textoBusqueda, fechaBusqueda, estadoBusqueda);

            if (auxListaTareas.Count>0)
            {
                this.dgResultadoBusqueda.ColumnHeadersVisible = true;
                this.dgResultadoBusqueda.Enabled = true;

                foreach (Tarea auxTarea in auxListaTareas)
                {
                    DataRow auxDataRow = auxDataTable.NewRow();
                    auxDataRow["Estado"] = auxTarea.EstadoTarea;
                    auxDataRow["Nombre"] = auxTarea.NombreTarea;
                    auxDataRow["Id"] = auxTarea.IdTarea;
                    auxDataRow["Fecha Creacion"] = auxTarea.FechaCreacionTarea;
                    auxDataTable.Rows.Add(auxDataRow);
                }

                this.dgResultadoBusqueda.DataSource = auxDataTable;



                this.dgResultadoBusqueda.Columns["Id"].Visible = false;
                this.dgResultadoBusqueda.Columns["Estado"].Width = 50;
                this.dgResultadoBusqueda.Columns["Estado"].ReadOnly = true;
                this.dgResultadoBusqueda.Columns["Nombre"].ReadOnly = true;

                this.dgResultadoBusqueda.Sort(this.dgResultadoBusqueda.Columns["Fecha Creacion"], ListSortDirection.Descending);
            }
            else
            {
                //Sin resultados
                auxDataTable = new DataTable();
                auxDataTable.Columns.Add("Resultados", typeof(String));

                DataRow auxDataRow = auxDataTable.NewRow();
                auxDataRow["Resultados"] = "No se han encontrado tareas con esas características.";
                auxDataTable.Rows.Add(auxDataRow);

                this.dgResultadoBusqueda.DataSource = auxDataTable;

                this.dgResultadoBusqueda.Columns["Resultados"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                this.dgResultadoBusqueda.ColumnHeadersVisible = false;
                this.dgResultadoBusqueda.Enabled = false;
                this.dgResultadoBusqueda.ClearSelection();
            }
            
        }


        /********** BOTONES ***********/
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Dispose();
            System.GC.Collect();
        }//Fin boton cerrar

        private void btnVerTarea_Click(object sender, EventArgs e)
        {
            if (dgResultadoBusqueda.RowCount > 0 && dgResultadoBusqueda.SelectedCells.Count > 0)
            {
                NegocioTarea auxNegocio = new NegocioTarea();
                Tarea auxTarea = new Tarea();
            
                int indiceFila = dgResultadoBusqueda.SelectedCells[0].RowIndex;
                DataGridViewRow filaSeleccionada = dgResultadoBusqueda.Rows[indiceFila];
            
                auxTarea = auxNegocio.buscarTarea(int.Parse(filaSeleccionada.Cells["Id"].Value.ToString()));

                PantallaVerTarea pVerTarea = new PantallaVerTarea(auxTarea, this);
                pVerTarea.ShowDialog();
            }
        }//Fin boton Ver tarea

        private void dgResultadoBusqueda_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            NegocioTarea auxNegocio = new NegocioTarea();
            Tarea auxTarea = new Tarea();

            int indiceFila = dgResultadoBusqueda.SelectedCells[0].RowIndex;
            DataGridViewRow filaSeleccionada = dgResultadoBusqueda.Rows[indiceFila];

            auxTarea = auxNegocio.buscarTarea(int.Parse(filaSeleccionada.Cells["Id"].Value.ToString()));

            PantallaVerTarea pVerTarea = new PantallaVerTarea(auxTarea, this);
            pVerTarea.ShowDialog();
        }//Fin Cell DoubleClick

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            cargarResultadosBusqueda();
            txtTextoBusqueda.Select();
        }//Fin Boton Buscar

        private void txtTextoBusqueda_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnBuscar_Click(sender, e);
            }
        }//Fin hotkey para buscar (Enter)


        /********** EVENTOS ***********/
        private void PantallaBuscarTarea_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.PInicio.cargarTareasPendientes();
        }
        
        private void chkPendientes_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkPendientes.Checked && !chkTerminadas.Checked)
            {
                chkTerminadas.Checked = true;
            }
            cargarResultadosBusqueda();
        }

        private void chkTerminadas_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkPendientes.Checked && !chkTerminadas.Checked)
            {
                chkPendientes.Checked = true;
            }
            cargarResultadosBusqueda();
        }

        private void cbxFechaCreacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            cargarResultadosBusqueda();
        }

    }
}

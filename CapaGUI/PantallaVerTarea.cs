﻿using CapaDTO;
using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaGUI
{
    public partial class PantallaVerTarea : Form
    {
        #region VARIABLES DE INSTANCIA, PROPERTIES

        private Tarea auxTarea;
        private PantallaInicio pInicio;
        private PantallaBuscarTarea pBuscar;
        private bool estadoBtnPrioridad;
        private bool estadoBtnRepetirTarea;
        private bool estadoBtnRecordatorio;
        private bool estadoTarea;
        private bool pnlRecordatorioOculto;
        private bool placeholderNombreTarea;
        private bool placeholderDetalleTarea;

        /********* PROPERTIES **********/
        public Tarea AuxTarea { get => auxTarea; set => auxTarea = value; }
        public PantallaInicio PInicio { get => pInicio; set => pInicio = value; }
        public PantallaBuscarTarea PBuscar { get => pBuscar; set => pBuscar = value; }
        public bool EstadoBtnPrioridad { get => estadoBtnPrioridad; set => estadoBtnPrioridad = value; }
        public bool EstadoBtnRepetirTarea { get => estadoBtnRepetirTarea; set => estadoBtnRepetirTarea = value; }
        public bool EstadoBtnRecordatorio { get => estadoBtnRecordatorio; set => estadoBtnRecordatorio = value; }
        public bool PnlRecordatorioOculto { get => pnlRecordatorioOculto; set => pnlRecordatorioOculto = value; }
        public bool PlaceholderNombreTarea { get => placeholderNombreTarea; set => placeholderNombreTarea = value; }
        public bool PlaceholderDetalleTarea { get => placeholderDetalleTarea; set => placeholderDetalleTarea = value; }
        public bool EstadoTarea { get => estadoTarea; set => estadoTarea = value; }

        /************* MOVER PANTALLA DESDE BARRA DE TITULO *************/
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture(); //Fin mover pantalla desde barra de titulo

        #endregion VARIABLES DE INSTANCIA, PROPERTIES


        /********* INICIALIZACION **********/
        public PantallaVerTarea(Tarea tarea, PantallaInicio pInicio)
        {
            InitializeComponent();
            this.AuxTarea = tarea;
            this.PInicio = pInicio;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PantallaVerTarea_FormClosing);
        }

        public PantallaVerTarea(Tarea tarea, PantallaBuscarTarea pBuscar)
        {
            InitializeComponent();
            this.AuxTarea = tarea;
            this.PBuscar = pBuscar;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PantallaVerTarea_FormClosing);
        }

        private void VerTarea_Load(object sender, EventArgs e)
        {
            //Cargar datos de Tarea
            this.txtNombreTarea.Text = this.AuxTarea.NombreTarea;
            this.txtNombreTarea.ForeColor = Color.Black;
            this.PlaceholderNombreTarea = false;
            
            if (this.AuxTarea.DetalleTarea == String.Empty)
            {
                this.txtDetalleTarea.Text = "Detalle";
                this.PlaceholderDetalleTarea = true;
            }
            else
            {
                this.txtDetalleTarea.Text = this.AuxTarea.DetalleTarea;
                this.txtDetalleTarea.ForeColor = Color.Black;
                this.PlaceholderDetalleTarea = false;
            }


            EstadoBtnPrioridad = this.AuxTarea.PrioridadTarea;
            if (EstadoBtnPrioridad)
            {
                this.btnPrioridad.Image = Properties.Resources.star;
                this.btnPrioridad.ForeColor = Color.White;
                this.btnPrioridad.BackColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnPrioridad.FlatAppearance.MouseDownBackColor = this.btnPrioridad.BackColor;
                this.btnPrioridad.FlatAppearance.MouseOverBackColor = this.btnPrioridad.BackColor;

            }
            EstadoBtnRepetirTarea = this.AuxTarea.Repetible;
            if (EstadoBtnRepetirTarea)
            {
                this.btnRepetirTarea.Image = Properties.Resources.repeat;
                this.btnRepetirTarea.ForeColor = Color.White;
                this.btnRepetirTarea.BackColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnRepetirTarea.FlatAppearance.MouseDownBackColor = this.btnRepetirTarea.BackColor;
                this.btnRepetirTarea.FlatAppearance.MouseOverBackColor = this.btnRepetirTarea.BackColor;
            }

            EstadoTarea = this.AuxTarea.EstadoTarea;
            if (EstadoTarea)
            {
                this.btnEstadoTarea.ForeColor = System.Drawing.Color.FromArgb(0, 149, 168);
            }
            else
            {
                this.btnEstadoTarea.BackColor = Color.White;
            }

            //Cargar recordatorio
            NegocioRecordatorio auxNegocioRecordatorio = new NegocioRecordatorio();
            Recordatorio auxRecordatorio = new Recordatorio();

            auxRecordatorio = auxNegocioRecordatorio.buscarRecordatorio(this.AuxTarea.IdTarea);

            if (auxRecordatorio.IdRecordatorio != 0)
            {
                //Cargar datos de recordatorio
                EstadoBtnRecordatorio = true;
                this.dtpDiaRecordatorio.Value = auxRecordatorio.FechaRecordatorio.Date;
                this.dtpHoraRecordatorio.Value = auxRecordatorio.FechaRecordatorio;
                PnlRecordatorioOculto = true;
                this.btnRecordatorio.Image = Properties.Resources.recordatorio;
                this.btnRecordatorio.ForeColor = Color.White;
                this.btnRecordatorio.BackColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnRecordatorio.FlatAppearance.MouseDownBackColor = this.btnRecordatorio.BackColor;
                this.btnRecordatorio.FlatAppearance.MouseOverBackColor = this.btnRecordatorio.BackColor;
                this.tmrAnimRecordatorio.Start();
            }
            else
            {
                EstadoBtnRecordatorio = false;
                PnlRecordatorioOculto = true;
            }

        }

        
        #region BOTONES

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Dispose();
            System.GC.Collect();
        }//Fin boton Salir

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //Guardar Datos
            NegocioTarea auxNegocioTarea = new NegocioTarea();
            this.AuxTarea.NombreTarea = txtNombreTarea.Text;
            if (PlaceholderDetalleTarea)
            {
                txtDetalleTarea.Text = String.Empty;
                this.AuxTarea.DetalleTarea = txtDetalleTarea.Text;
            }
            else
            {
                this.AuxTarea.DetalleTarea = txtDetalleTarea.Text;
            }
            this.AuxTarea.PrioridadTarea = EstadoBtnPrioridad;
            this.AuxTarea.Repetible = EstadoBtnRepetirTarea;
            this.AuxTarea.EstadoTarea = EstadoTarea;

            auxNegocioTarea.modificarTarea(this.AuxTarea);

            //Agregar recordatorio
            if (EstadoBtnRecordatorio && this.AuxTarea.EstadoTarea == false)
            {
                NegocioRecordatorio auxNegocioRecordatorio = new NegocioRecordatorio();
                Recordatorio auxRecordatorio = new Recordatorio();

                DateTime auxDiaRecordatorio = this.dtpDiaRecordatorio.Value;
                DateTime auxHoraRecordatorio = this.dtpHoraRecordatorio.Value;
                DateTime auxFechaRecordatorio = auxDiaRecordatorio.Date + auxHoraRecordatorio.TimeOfDay; //Quitar segundos

                auxRecordatorio.FechaRecordatorio = auxFechaRecordatorio;

                auxRecordatorio.RepetirRecordatorio = false;
                auxRecordatorio.IdTarea = this.AuxTarea.IdTarea;

                //Evaluar si la tarea posee un recordatorio
                int idRecordatorio = auxNegocioRecordatorio.buscarRecordatorio(auxRecordatorio.IdTarea).IdRecordatorio;

                if (idRecordatorio != 0)
                {
                    //Modificar recordatorio existente
                    auxRecordatorio.IdRecordatorio = idRecordatorio;
                    auxRecordatorio.EstadoRecordatorio = false;
                    auxNegocioRecordatorio.modificarRecordatorio(auxRecordatorio);
                }
                else
                {
                    //Crear recordatorio
                    auxNegocioRecordatorio.insertarRecordatorio(auxRecordatorio);
                }
            }

            //Quitar recordatorio
            if (!EstadoBtnRecordatorio && this.AuxTarea.EstadoTarea == false)
            {
                NegocioRecordatorio auxNegocioRecordatorio = new NegocioRecordatorio();
                Recordatorio auxRecordatorio = new Recordatorio();

                auxRecordatorio.IdTarea = this.AuxTarea.IdTarea;

                //Evaluar si la tarea posee un recordatorio
                int idRecordatorio = auxNegocioRecordatorio.buscarRecordatorio(auxRecordatorio.IdTarea).IdRecordatorio;

                if (idRecordatorio != 0)
                {
                    auxNegocioRecordatorio.eliminarRecordatorio(idRecordatorio);
                }
            }

            this.Dispose();
            System.GC.Collect();

        }//Fin boton Guardar Cambios

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Dispose();
            System.GC.Collect();
        }//Fin Boton cerrar

        private void btnEliminarTarea_Click(object sender, EventArgs e)
        {
            //Eliminar Tarea
            DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea eliminar la tarea?", "Confirmación", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                NegocioTarea auxNegocioTarea = new NegocioTarea();
                auxNegocioTarea.eliminarTarea(this.AuxTarea.IdTarea);

                NegocioRecordatorio auxNegocioRecordatorio = new NegocioRecordatorio();
                if (auxNegocioRecordatorio.buscarRecordatorio(this.AuxTarea.IdTarea) != null)
                {
                    int idRecordatorio = auxNegocioRecordatorio.buscarRecordatorio(this.AuxTarea.IdTarea).IdRecordatorio;
                    auxNegocioRecordatorio.eliminarRecordatorio(idRecordatorio);
                    PInicio.cargarRecordatorios();
                }
                this.Dispose();
                System.GC.Collect();
            }
        } //Fin Boton Eliminar Tarea

        private void btnPrioridad_Click(object sender, EventArgs e)
        {
            if (EstadoBtnPrioridad)
            {
                this.btnPrioridad.Image = Properties.Resources.star_borde_amarillo;
                this.btnPrioridad.ForeColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnPrioridad.BackColor = Color.White;
                this.btnPrioridad.FlatAppearance.MouseDownBackColor = this.btnPrioridad.BackColor;
                this.btnPrioridad.FlatAppearance.MouseOverBackColor = this.btnPrioridad.BackColor;
                this.EstadoBtnPrioridad = false;
            }
            else
            {
                this.btnPrioridad.Image = Properties.Resources.star;
                this.btnPrioridad.ForeColor = Color.White;
                this.btnPrioridad.BackColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnPrioridad.FlatAppearance.MouseDownBackColor = this.btnPrioridad.BackColor;
                this.btnPrioridad.FlatAppearance.MouseOverBackColor = this.btnPrioridad.BackColor;
                this.EstadoBtnPrioridad = true;
            }
        } //Fin Boton Prioridad

        private void btnRepetirTarea_Click(object sender, EventArgs e)
        {
            if (estadoBtnRepetirTarea)
            {
                this.btnRepetirTarea.Image = Properties.Resources.repeat_color;
                this.btnRepetirTarea.ForeColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnRepetirTarea.BackColor = Color.White;
                this.btnRepetirTarea.FlatAppearance.MouseDownBackColor = this.btnRepetirTarea.BackColor;
                this.btnRepetirTarea.FlatAppearance.MouseOverBackColor = this.btnRepetirTarea.BackColor;
                this.EstadoBtnRepetirTarea = false;
            }
            else
            {
                this.btnRepetirTarea.Image = Properties.Resources.repeat;
                this.btnRepetirTarea.ForeColor = Color.White;
                this.btnRepetirTarea.BackColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnRepetirTarea.FlatAppearance.MouseDownBackColor = this.btnRepetirTarea.BackColor;
                this.btnRepetirTarea.FlatAppearance.MouseOverBackColor = this.btnRepetirTarea.BackColor;
                this.EstadoBtnRepetirTarea = true;
            }
        } //Fin Boton Repetir Tarea

        private void btnRecordatorio_Click(object sender, EventArgs e)
        {
            if (EstadoBtnRecordatorio)
            {
                this.btnRecordatorio.Image = Properties.Resources.recordatorio_color;
                this.btnRecordatorio.ForeColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnRecordatorio.BackColor = Color.White;
                this.btnRecordatorio.FlatAppearance.MouseDownBackColor = this.btnRecordatorio.BackColor;
                this.btnRecordatorio.FlatAppearance.MouseOverBackColor = this.btnRecordatorio.BackColor;
                this.EstadoBtnRecordatorio = false;
                this.tmrAnimRecordatorio.Start();
            }
            else
            {
                this.btnRecordatorio.Image = Properties.Resources.recordatorio;
                this.btnRecordatorio.ForeColor = Color.White;
                this.btnRecordatorio.BackColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnRecordatorio.FlatAppearance.MouseDownBackColor = this.btnRecordatorio.BackColor;
                this.btnRecordatorio.FlatAppearance.MouseOverBackColor = this.btnRecordatorio.BackColor;
                this.EstadoBtnRecordatorio = true;
                this.tmrAnimRecordatorio.Start();
            }
        } //Fin Boton Recordatorio

        private void btnEstadoTarea_Click(object sender, EventArgs e)
        {
            if (EstadoTarea)
            {
                Button auxButton = (Button)sender;
                auxButton.FlatAppearance.MouseOverBackColor = Color.White;
                auxButton.FlatAppearance.MouseDownBackColor = Color.White;
                this.btnEstadoTarea.BackColor = Color.White;
                EstadoTarea = false;
            }
            else
            {
                Button auxButton = (Button)sender;
                auxButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(0, 149, 168);
                auxButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(0, 149, 168);
                this.btnEstadoTarea.BackColor = System.Drawing.Color.FromArgb(0, 149, 168);
                EstadoTarea = true;
            }

        }//Fin Boton Estado Tarea

        #endregion BOTONES

        
        #region GUI

        /********** EFECTOS HOVER ***********/
        private void btnPrioridad_MouseEnter(object sender, EventArgs e)
        {
            Button auxButton = (Button)sender;
            auxButton.FlatAppearance.MouseOverBackColor = this.btnPrioridad.BackColor;
            auxButton.FlatAppearance.MouseDownBackColor = this.btnPrioridad.BackColor;
        }

        private void btnRepetirTarea_MouseEnter(object sender, EventArgs e)
        {
            Button auxButton = (Button)sender;
            auxButton.FlatAppearance.MouseOverBackColor = this.btnRepetirTarea.BackColor;
            auxButton.FlatAppearance.MouseDownBackColor = this.btnRepetirTarea.BackColor;
        }

        private void btnRecordatorio_MouseEnter(object sender, EventArgs e)
        {
            Button auxButton = (Button)sender;
            auxButton.FlatAppearance.MouseOverBackColor = this.btnRecordatorio.BackColor;
            auxButton.FlatAppearance.MouseDownBackColor = this.btnRecordatorio.BackColor;
        }

        private void btnEstadoTarea_MouseEnter(object sender, EventArgs e)
        {
            if (!EstadoTarea)
            {
                Button auxButton = (Button)sender;
                auxButton.FlatAppearance.MouseOverBackColor = Color.White;
                auxButton.FlatAppearance.MouseDownBackColor = Color.White;
            }
        }


        /************ PLACEHOLDER ************/
        private void txtNombreTarea_Enter(object sender, EventArgs e)
        {
            if (this.txtNombreTarea.Text == "Tarea" && PlaceholderNombreTarea)
            {
                this.txtNombreTarea.Text = String.Empty;
                this.txtNombreTarea.ForeColor = Color.Black;
            }
        }

        private void txtNombreTarea_Leave(object sender, EventArgs e)
        {
            if (this.txtNombreTarea.Text == String.Empty)
            {
                this.txtNombreTarea.Text = "Tarea";
                this.txtNombreTarea.ForeColor = Color.DarkGray;
                PlaceholderNombreTarea = true;
            }
        }

        private void txtNombreTarea_TextChanged(object sender, EventArgs e)
        {
            if (this.txtNombreTarea.Text == String.Empty && PlaceholderNombreTarea)
            {
                PlaceholderNombreTarea = false;
            }
        }

        private void txtDetalleTarea_Enter(object sender, EventArgs e)
        {
            if (this.txtDetalleTarea.Text == "Detalle" && PlaceholderDetalleTarea)
            {
                this.txtDetalleTarea.Text = String.Empty;
                this.txtDetalleTarea.ForeColor = Color.Black;
            }
        }

        private void txtDetalleTarea_Leave(object sender, EventArgs e)
        {
            if (this.txtDetalleTarea.Text == String.Empty)
            {
                this.txtDetalleTarea.Text = "Detalle";
                this.txtDetalleTarea.ForeColor = Color.DarkGray;
                PlaceholderDetalleTarea = true;
            }
        }

        private void txtDetalleTarea_TextChanged(object sender, EventArgs e)
        {
            if (this.txtDetalleTarea.Text == String.Empty && PlaceholderDetalleTarea)
            {
                PlaceholderDetalleTarea = false;
            }
        }


        /************ MOVER VENTANA DESDE BARRA DE TITULO ************/
        private void pnlTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void lblTitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        #endregion GUI


        /********** EVENTOS ***********/
        private void PantallaVerTarea_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (PBuscar == null)
            {
                this.PInicio.cargarTareasPendientes();
                this.PInicio.cargarRecordatorios();
            }
            else
            {
                this.PBuscar.cargarResultadosBusqueda();
            }
        }

        private void tmrAnimRecordatorio_Tick(object sender, EventArgs e)
        {
            if (PnlRecordatorioOculto)
            {
                this.pnlRecordatorio.Height = this.pnlRecordatorio.Height + 10;
                this.btnEliminarTarea.Location = new Point(this.btnEliminarTarea.Location.X, this.btnEliminarTarea.Location.Y + 10);
                this.btnGuardar.Location = new Point(this.btnGuardar.Location.X, this.btnGuardar.Location.Y + 10);
                this.Height = this.Height + 10;

                if (this.pnlRecordatorio.Height >= 110)
                {
                    this.tmrAnimRecordatorio.Stop();
                    this.Refresh();
                    this.PnlRecordatorioOculto = false;
                }
            }
            else
            {
                this.pnlRecordatorio.Height = this.pnlRecordatorio.Height - 10;
                this.btnEliminarTarea.Location = new Point(this.btnEliminarTarea.Location.X, this.btnEliminarTarea.Location.Y - 10);
                this.btnGuardar.Location = new Point(this.btnGuardar.Location.X, this.btnGuardar.Location.Y - 10);
                this.Height = this.Height - 10;

                if (this.pnlRecordatorio.Height <= 0)
                {
                    this.tmrAnimRecordatorio.Stop();
                    this.Refresh();
                    this.PnlRecordatorioOculto = true;
                }
            }
        }

    }
}

﻿namespace CapaGUI
{
    partial class PantallaInicio
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PantallaInicio));
            this.pnlSideMenu = new System.Windows.Forms.Panel();
            this.pnlSelected = new System.Windows.Forms.Panel();
            this.pnlSideHeaderLogo = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.btnConfiguracion = new System.Windows.Forms.Button();
            this.btnContactos = new System.Windows.Forms.Button();
            this.btnMailFacturas = new System.Windows.Forms.Button();
            this.btnFacturacion = new System.Windows.Forms.Button();
            this.btnTareas = new System.Windows.Forms.Button();
            this.pnlTitleBar = new System.Windows.Forms.Panel();
            this.lblTitleBar = new System.Windows.Forms.Label();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.btnMinimizar = new System.Windows.Forms.Button();
            this.btnEliminarTareaPendiente = new System.Windows.Forms.Button();
            this.btnVerTareaPendiente = new System.Windows.Forms.Button();
            this.dgTareasPendientes = new System.Windows.Forms.DataGridView();
            this.btnBuscarTarea = new System.Windows.Forms.Button();
            this.btnNuevaTarea = new System.Windows.Forms.Button();
            this.tmrTextBox = new System.Windows.Forms.Timer(this.components);
            this.pnlTextBoxTareas = new System.Windows.Forms.Panel();
            this.txtInputText = new System.Windows.Forms.TextBox();
            this.tmrRecordatorios = new System.Windows.Forms.Timer(this.components);
            this.tmrBtnNuevaTarea = new System.Windows.Forms.Timer(this.components);
            this.tmrBtnBuscar = new System.Windows.Forms.Timer(this.components);
            this.tbcPestanas = new System.Windows.Forms.TabControl();
            this.tabTareas = new System.Windows.Forms.TabPage();
            this.tabFacturacion = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.tabMailFacturas = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.tabContactos = new System.Windows.Forms.TabPage();
            this.btnEliminarContacto = new System.Windows.Forms.Button();
            this.btnModificarContacto = new System.Windows.Forms.Button();
            this.pnlTextBoxContactos = new System.Windows.Forms.Panel();
            this.txtBuscarContacto = new System.Windows.Forms.TextBox();
            this.btnAgregarContacto = new System.Windows.Forms.Button();
            this.btnBuscarContacto = new System.Windows.Forms.Button();
            this.dgContactos = new System.Windows.Forms.DataGridView();
            this.tabConfiguracion = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbExportar = new System.Windows.Forms.RadioButton();
            this.btnProcesar = new System.Windows.Forms.Button();
            this.rbImportar = new System.Windows.Forms.RadioButton();
            this.cbxSeleccionarTabla = new System.Windows.Forms.ComboBox();
            this.pnlSideMenu.SuspendLayout();
            this.pnlSideHeaderLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.pnlTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTareasPendientes)).BeginInit();
            this.pnlTextBoxTareas.SuspendLayout();
            this.tbcPestanas.SuspendLayout();
            this.tabTareas.SuspendLayout();
            this.tabFacturacion.SuspendLayout();
            this.tabMailFacturas.SuspendLayout();
            this.tabContactos.SuspendLayout();
            this.pnlTextBoxContactos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgContactos)).BeginInit();
            this.tabConfiguracion.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSideMenu
            // 
            this.pnlSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.pnlSideMenu.Controls.Add(this.pnlSelected);
            this.pnlSideMenu.Controls.Add(this.pnlSideHeaderLogo);
            this.pnlSideMenu.Controls.Add(this.btnConfiguracion);
            this.pnlSideMenu.Controls.Add(this.btnContactos);
            this.pnlSideMenu.Controls.Add(this.btnMailFacturas);
            this.pnlSideMenu.Controls.Add(this.btnFacturacion);
            this.pnlSideMenu.Controls.Add(this.btnTareas);
            this.pnlSideMenu.Location = new System.Drawing.Point(0, 35);
            this.pnlSideMenu.Margin = new System.Windows.Forms.Padding(0);
            this.pnlSideMenu.Name = "pnlSideMenu";
            this.pnlSideMenu.Size = new System.Drawing.Size(250, 665);
            this.pnlSideMenu.TabIndex = 2;
            // 
            // pnlSelected
            // 
            this.pnlSelected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.pnlSelected.Location = new System.Drawing.Point(0, 129);
            this.pnlSelected.Margin = new System.Windows.Forms.Padding(0);
            this.pnlSelected.Name = "pnlSelected";
            this.pnlSelected.Size = new System.Drawing.Size(0, 76);
            this.pnlSelected.TabIndex = 6;
            // 
            // pnlSideHeaderLogo
            // 
            this.pnlSideHeaderLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.pnlSideHeaderLogo.Controls.Add(this.label2);
            this.pnlSideHeaderLogo.Controls.Add(this.label1);
            this.pnlSideHeaderLogo.Controls.Add(this.picLogo);
            this.pnlSideHeaderLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSideHeaderLogo.Location = new System.Drawing.Point(0, 0);
            this.pnlSideHeaderLogo.Name = "pnlSideHeaderLogo";
            this.pnlSideHeaderLogo.Size = new System.Drawing.Size(250, 110);
            this.pnlSideHeaderLogo.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Montserrat Medium", 13F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(100, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "EDUCATIVO";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Montserrat Medium", 13F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(100, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "PUNTO";
            // 
            // picLogo
            // 
            this.picLogo.Image = global::CapaGUI.Properties.Resources.logo;
            this.picLogo.Location = new System.Drawing.Point(22, 18);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(72, 72);
            this.picLogo.TabIndex = 0;
            this.picLogo.TabStop = false;
            // 
            // btnConfiguracion
            // 
            this.btnConfiguracion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.btnConfiguracion.FlatAppearance.BorderSize = 0;
            this.btnConfiguracion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(49)))), ((int)(((byte)(63)))));
            this.btnConfiguracion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(49)))), ((int)(((byte)(63)))));
            this.btnConfiguracion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfiguracion.Font = new System.Drawing.Font("Montserrat Medium", 10F);
            this.btnConfiguracion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(137)))), ((int)(((byte)(146)))));
            this.btnConfiguracion.Image = global::CapaGUI.Properties.Resources.settings_color;
            this.btnConfiguracion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfiguracion.Location = new System.Drawing.Point(0, 570);
            this.btnConfiguracion.Margin = new System.Windows.Forms.Padding(0);
            this.btnConfiguracion.Name = "btnConfiguracion";
            this.btnConfiguracion.Padding = new System.Windows.Forms.Padding(18, 0, 0, 0);
            this.btnConfiguracion.Size = new System.Drawing.Size(250, 76);
            this.btnConfiguracion.TabIndex = 4;
            this.btnConfiguracion.Text = "    CONFIGURACIÓN";
            this.btnConfiguracion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnConfiguracion.UseVisualStyleBackColor = false;
            this.btnConfiguracion.Click += new System.EventHandler(this.btnConfiguracion_Click);
            this.btnConfiguracion.MouseEnter += new System.EventHandler(this.btnConfiguracion_MouseEnter);
            this.btnConfiguracion.MouseLeave += new System.EventHandler(this.btnConfiguracion_MouseLeave);
            // 
            // btnContactos
            // 
            this.btnContactos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.btnContactos.FlatAppearance.BorderSize = 0;
            this.btnContactos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(49)))), ((int)(((byte)(63)))));
            this.btnContactos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(49)))), ((int)(((byte)(63)))));
            this.btnContactos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnContactos.Font = new System.Drawing.Font("Montserrat Medium", 10F);
            this.btnContactos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(137)))), ((int)(((byte)(146)))));
            this.btnContactos.Image = global::CapaGUI.Properties.Resources.contacts_color;
            this.btnContactos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnContactos.Location = new System.Drawing.Point(0, 357);
            this.btnContactos.Margin = new System.Windows.Forms.Padding(0);
            this.btnContactos.Name = "btnContactos";
            this.btnContactos.Padding = new System.Windows.Forms.Padding(18, 0, 0, 0);
            this.btnContactos.Size = new System.Drawing.Size(250, 76);
            this.btnContactos.TabIndex = 3;
            this.btnContactos.Text = "    CONTACTOS";
            this.btnContactos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnContactos.UseVisualStyleBackColor = false;
            this.btnContactos.Click += new System.EventHandler(this.btnContactos_Click);
            this.btnContactos.MouseEnter += new System.EventHandler(this.btnContactos_MouseEnter);
            this.btnContactos.MouseLeave += new System.EventHandler(this.btnContactos_MouseLeave);
            // 
            // btnMailFacturas
            // 
            this.btnMailFacturas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.btnMailFacturas.FlatAppearance.BorderSize = 0;
            this.btnMailFacturas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(49)))), ((int)(((byte)(63)))));
            this.btnMailFacturas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(49)))), ((int)(((byte)(63)))));
            this.btnMailFacturas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMailFacturas.Font = new System.Drawing.Font("Montserrat Medium", 10F);
            this.btnMailFacturas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(137)))), ((int)(((byte)(146)))));
            this.btnMailFacturas.Image = global::CapaGUI.Properties.Resources.mail_color;
            this.btnMailFacturas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMailFacturas.Location = new System.Drawing.Point(0, 281);
            this.btnMailFacturas.Margin = new System.Windows.Forms.Padding(0);
            this.btnMailFacturas.Name = "btnMailFacturas";
            this.btnMailFacturas.Padding = new System.Windows.Forms.Padding(18, 0, 0, 0);
            this.btnMailFacturas.Size = new System.Drawing.Size(250, 76);
            this.btnMailFacturas.TabIndex = 2;
            this.btnMailFacturas.Text = "    MAIL FACTURAS";
            this.btnMailFacturas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMailFacturas.UseVisualStyleBackColor = false;
            this.btnMailFacturas.Click += new System.EventHandler(this.btnMailFacturas_Click);
            this.btnMailFacturas.MouseEnter += new System.EventHandler(this.btnMailFacturas_MouseEnter);
            this.btnMailFacturas.MouseLeave += new System.EventHandler(this.btnMailFacturas_MouseLeave);
            // 
            // btnFacturacion
            // 
            this.btnFacturacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.btnFacturacion.FlatAppearance.BorderSize = 0;
            this.btnFacturacion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(49)))), ((int)(((byte)(63)))));
            this.btnFacturacion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(49)))), ((int)(((byte)(63)))));
            this.btnFacturacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFacturacion.Font = new System.Drawing.Font("Montserrat Medium", 10F);
            this.btnFacturacion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(137)))), ((int)(((byte)(146)))));
            this.btnFacturacion.Image = global::CapaGUI.Properties.Resources.factura_color;
            this.btnFacturacion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFacturacion.Location = new System.Drawing.Point(0, 205);
            this.btnFacturacion.Margin = new System.Windows.Forms.Padding(0);
            this.btnFacturacion.Name = "btnFacturacion";
            this.btnFacturacion.Padding = new System.Windows.Forms.Padding(18, 0, 0, 0);
            this.btnFacturacion.Size = new System.Drawing.Size(250, 76);
            this.btnFacturacion.TabIndex = 1;
            this.btnFacturacion.Text = "    FACTURACIÓN";
            this.btnFacturacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnFacturacion.UseVisualStyleBackColor = false;
            this.btnFacturacion.Click += new System.EventHandler(this.btnFacturacion_Click);
            this.btnFacturacion.MouseEnter += new System.EventHandler(this.btnFacturacion_MouseEnter);
            this.btnFacturacion.MouseLeave += new System.EventHandler(this.btnFacturacion_MouseLeave);
            // 
            // btnTareas
            // 
            this.btnTareas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.btnTareas.FlatAppearance.BorderSize = 0;
            this.btnTareas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(49)))), ((int)(((byte)(63)))));
            this.btnTareas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(49)))), ((int)(((byte)(63)))));
            this.btnTareas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTareas.Font = new System.Drawing.Font("Montserrat Medium", 10F);
            this.btnTareas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(137)))), ((int)(((byte)(146)))));
            this.btnTareas.Image = global::CapaGUI.Properties.Resources.to_do_color;
            this.btnTareas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTareas.Location = new System.Drawing.Point(0, 129);
            this.btnTareas.Margin = new System.Windows.Forms.Padding(0);
            this.btnTareas.Name = "btnTareas";
            this.btnTareas.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnTareas.Size = new System.Drawing.Size(250, 76);
            this.btnTareas.TabIndex = 0;
            this.btnTareas.Text = "    TAREAS";
            this.btnTareas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTareas.UseVisualStyleBackColor = false;
            this.btnTareas.Click += new System.EventHandler(this.btnTareas_Click);
            this.btnTareas.MouseEnter += new System.EventHandler(this.btnTareas_MouseEnter);
            this.btnTareas.MouseLeave += new System.EventHandler(this.btnTareas_MouseLeave);
            // 
            // pnlTitleBar
            // 
            this.pnlTitleBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(49)))), ((int)(((byte)(63)))));
            this.pnlTitleBar.Controls.Add(this.lblTitleBar);
            this.pnlTitleBar.Controls.Add(this.btnCerrar);
            this.pnlTitleBar.Controls.Add(this.btnMinimizar);
            this.pnlTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitleBar.Location = new System.Drawing.Point(0, 0);
            this.pnlTitleBar.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTitleBar.Name = "pnlTitleBar";
            this.pnlTitleBar.Size = new System.Drawing.Size(660, 35);
            this.pnlTitleBar.TabIndex = 5;
            this.pnlTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitleBar_MouseDown);
            // 
            // lblTitleBar
            // 
            this.lblTitleBar.AutoSize = true;
            this.lblTitleBar.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblTitleBar.ForeColor = System.Drawing.Color.White;
            this.lblTitleBar.Location = new System.Drawing.Point(12, 9);
            this.lblTitleBar.Name = "lblTitleBar";
            this.lblTitleBar.Size = new System.Drawing.Size(264, 19);
            this.lblTitleBar.TabIndex = 2;
            this.lblTitleBar.Text = "Gestión de Tareas - Punto Educativo 1.5.0";
            this.lblTitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lblTitleBar_MouseDown);
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(0)))), ((int)(((byte)(15)))));
            this.btnCerrar.FlatAppearance.BorderSize = 0;
            this.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.ForeColor = System.Drawing.Color.White;
            this.btnCerrar.Image = global::CapaGUI.Properties.Resources.cerrar;
            this.btnCerrar.Location = new System.Drawing.Point(605, 5);
            this.btnCerrar.Margin = new System.Windows.Forms.Padding(5);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(50, 25);
            this.btnCerrar.TabIndex = 0;
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.btnMinimizar.FlatAppearance.BorderSize = 0;
            this.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimizar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimizar.ForeColor = System.Drawing.Color.White;
            this.btnMinimizar.Location = new System.Drawing.Point(550, 5);
            this.btnMinimizar.Margin = new System.Windows.Forms.Padding(5, 5, 0, 5);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(50, 25);
            this.btnMinimizar.TabIndex = 1;
            this.btnMinimizar.Text = "__";
            this.btnMinimizar.UseVisualStyleBackColor = false;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // btnEliminarTareaPendiente
            // 
            this.btnEliminarTareaPendiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnEliminarTareaPendiente.FlatAppearance.BorderSize = 0;
            this.btnEliminarTareaPendiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarTareaPendiente.Font = new System.Drawing.Font("Montserrat Medium", 12F);
            this.btnEliminarTareaPendiente.ForeColor = System.Drawing.Color.White;
            this.btnEliminarTareaPendiente.Location = new System.Drawing.Point(207, 606);
            this.btnEliminarTareaPendiente.Margin = new System.Windows.Forms.Padding(0, 15, 0, 10);
            this.btnEliminarTareaPendiente.Name = "btnEliminarTareaPendiente";
            this.btnEliminarTareaPendiente.Size = new System.Drawing.Size(185, 40);
            this.btnEliminarTareaPendiente.TabIndex = 11;
            this.btnEliminarTareaPendiente.Text = "ELIMINAR";
            this.btnEliminarTareaPendiente.UseVisualStyleBackColor = false;
            this.btnEliminarTareaPendiente.Click += new System.EventHandler(this.btnEliminarTareaPendiente_Click);
            // 
            // btnVerTareaPendiente
            // 
            this.btnVerTareaPendiente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnVerTareaPendiente.FlatAppearance.BorderSize = 0;
            this.btnVerTareaPendiente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVerTareaPendiente.Font = new System.Drawing.Font("Montserrat Medium", 12F);
            this.btnVerTareaPendiente.ForeColor = System.Drawing.Color.White;
            this.btnVerTareaPendiente.Location = new System.Drawing.Point(10, 606);
            this.btnVerTareaPendiente.Margin = new System.Windows.Forms.Padding(0, 15, 0, 10);
            this.btnVerTareaPendiente.Name = "btnVerTareaPendiente";
            this.btnVerTareaPendiente.Size = new System.Drawing.Size(185, 40);
            this.btnVerTareaPendiente.TabIndex = 10;
            this.btnVerTareaPendiente.Text = "VER TAREA";
            this.btnVerTareaPendiente.UseVisualStyleBackColor = false;
            this.btnVerTareaPendiente.Click += new System.EventHandler(this.btnVerTareaPendiente_Click);
            // 
            // dgTareasPendientes
            // 
            this.dgTareasPendientes.AllowUserToAddRows = false;
            this.dgTareasPendientes.AllowUserToDeleteRows = false;
            this.dgTareasPendientes.AllowUserToResizeColumns = false;
            this.dgTareasPendientes.AllowUserToResizeRows = false;
            this.dgTareasPendientes.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgTareasPendientes.BackgroundColor = System.Drawing.Color.White;
            this.dgTareasPendientes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgTareasPendientes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgTareasPendientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTareasPendientes.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(10, 10, 5, 10);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgTareasPendientes.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgTareasPendientes.Location = new System.Drawing.Point(10, 65);
            this.dgTareasPendientes.Margin = new System.Windows.Forms.Padding(0);
            this.dgTareasPendientes.MultiSelect = false;
            this.dgTareasPendientes.Name = "dgTareasPendientes";
            this.dgTareasPendientes.RowHeadersVisible = false;
            this.dgTareasPendientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTareasPendientes.Size = new System.Drawing.Size(382, 526);
            this.dgTareasPendientes.TabIndex = 9;
            this.dgTareasPendientes.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgTareasPendientes_CellDoubleClick);
            this.dgTareasPendientes.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgTareasPendientes_CellMouseUp);
            this.dgTareasPendientes.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgTareasPendientes_CellValueChanged);
            // 
            // btnBuscarTarea
            // 
            this.btnBuscarTarea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnBuscarTarea.FlatAppearance.BorderSize = 0;
            this.btnBuscarTarea.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarTarea.Font = new System.Drawing.Font("Montserrat Medium", 12F);
            this.btnBuscarTarea.ForeColor = System.Drawing.Color.White;
            this.btnBuscarTarea.Location = new System.Drawing.Point(207, 10);
            this.btnBuscarTarea.Margin = new System.Windows.Forms.Padding(7, 0, 0, 15);
            this.btnBuscarTarea.Name = "btnBuscarTarea";
            this.btnBuscarTarea.Size = new System.Drawing.Size(185, 40);
            this.btnBuscarTarea.TabIndex = 8;
            this.btnBuscarTarea.Text = "BUSCAR";
            this.btnBuscarTarea.UseVisualStyleBackColor = false;
            this.btnBuscarTarea.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnNuevaTarea
            // 
            this.btnNuevaTarea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnNuevaTarea.FlatAppearance.BorderSize = 0;
            this.btnNuevaTarea.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevaTarea.Font = new System.Drawing.Font("Montserrat Medium", 12F);
            this.btnNuevaTarea.ForeColor = System.Drawing.Color.White;
            this.btnNuevaTarea.Location = new System.Drawing.Point(10, 10);
            this.btnNuevaTarea.Margin = new System.Windows.Forms.Padding(0, 0, 7, 15);
            this.btnNuevaTarea.Name = "btnNuevaTarea";
            this.btnNuevaTarea.Size = new System.Drawing.Size(185, 40);
            this.btnNuevaTarea.TabIndex = 7;
            this.btnNuevaTarea.Text = "NUEVA";
            this.btnNuevaTarea.UseVisualStyleBackColor = false;
            this.btnNuevaTarea.Click += new System.EventHandler(this.btnNuevaTarea_Click);
            // 
            // tmrTextBox
            // 
            this.tmrTextBox.Interval = 10;
            this.tmrTextBox.Tick += new System.EventHandler(this.tmrTextBox_Tick);
            // 
            // pnlTextBoxTareas
            // 
            this.pnlTextBoxTareas.BackColor = System.Drawing.Color.White;
            this.pnlTextBoxTareas.Controls.Add(this.txtInputText);
            this.pnlTextBoxTareas.Location = new System.Drawing.Point(10, 65);
            this.pnlTextBoxTareas.Margin = new System.Windows.Forms.Padding(15, 0, 15, 15);
            this.pnlTextBoxTareas.Name = "pnlTextBoxTareas";
            this.pnlTextBoxTareas.Size = new System.Drawing.Size(382, 0);
            this.pnlTextBoxTareas.TabIndex = 12;
            // 
            // txtInputText
            // 
            this.txtInputText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtInputText.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInputText.Location = new System.Drawing.Point(10, 9);
            this.txtInputText.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.txtInputText.Name = "txtInputText";
            this.txtInputText.Size = new System.Drawing.Size(362, 26);
            this.txtInputText.TabIndex = 2;
            this.txtInputText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtInputText_KeyDown);
            // 
            // tmrRecordatorios
            // 
            this.tmrRecordatorios.Tick += new System.EventHandler(this.tmrRecordatorios_Tick);
            // 
            // tmrBtnNuevaTarea
            // 
            this.tmrBtnNuevaTarea.Tick += new System.EventHandler(this.tmrBtnNuevaTarea_Tick);
            // 
            // tmrBtnBuscar
            // 
            this.tmrBtnBuscar.Tick += new System.EventHandler(this.tmrBtnBuscar_Tick);
            // 
            // tbcPestanas
            // 
            this.tbcPestanas.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tbcPestanas.Controls.Add(this.tabTareas);
            this.tbcPestanas.Controls.Add(this.tabFacturacion);
            this.tbcPestanas.Controls.Add(this.tabMailFacturas);
            this.tbcPestanas.Controls.Add(this.tabContactos);
            this.tbcPestanas.Controls.Add(this.tabConfiguracion);
            this.tbcPestanas.ItemSize = new System.Drawing.Size(0, 1);
            this.tbcPestanas.Location = new System.Drawing.Point(250, 35);
            this.tbcPestanas.Margin = new System.Windows.Forms.Padding(0);
            this.tbcPestanas.Name = "tbcPestanas";
            this.tbcPestanas.SelectedIndex = 0;
            this.tbcPestanas.Size = new System.Drawing.Size(410, 665);
            this.tbcPestanas.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tbcPestanas.TabIndex = 13;
            // 
            // tabTareas
            // 
            this.tabTareas.BackColor = System.Drawing.SystemColors.Control;
            this.tabTareas.Controls.Add(this.dgTareasPendientes);
            this.tabTareas.Controls.Add(this.pnlTextBoxTareas);
            this.tabTareas.Controls.Add(this.btnNuevaTarea);
            this.tabTareas.Controls.Add(this.btnEliminarTareaPendiente);
            this.tabTareas.Controls.Add(this.btnBuscarTarea);
            this.tabTareas.Controls.Add(this.btnVerTareaPendiente);
            this.tabTareas.Location = new System.Drawing.Point(4, 5);
            this.tabTareas.Margin = new System.Windows.Forms.Padding(0);
            this.tabTareas.Name = "tabTareas";
            this.tabTareas.Size = new System.Drawing.Size(402, 656);
            this.tabTareas.TabIndex = 0;
            this.tabTareas.Text = "tabPage1";
            // 
            // tabFacturacion
            // 
            this.tabFacturacion.Controls.Add(this.label6);
            this.tabFacturacion.Location = new System.Drawing.Point(4, 5);
            this.tabFacturacion.Name = "tabFacturacion";
            this.tabFacturacion.Padding = new System.Windows.Forms.Padding(3);
            this.tabFacturacion.Size = new System.Drawing.Size(402, 656);
            this.tabFacturacion.TabIndex = 1;
            this.tabFacturacion.Text = "tabPage2";
            this.tabFacturacion.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(154, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Facturacion";
            // 
            // tabMailFacturas
            // 
            this.tabMailFacturas.Controls.Add(this.label5);
            this.tabMailFacturas.Location = new System.Drawing.Point(4, 5);
            this.tabMailFacturas.Name = "tabMailFacturas";
            this.tabMailFacturas.Size = new System.Drawing.Size(402, 656);
            this.tabMailFacturas.TabIndex = 2;
            this.tabMailFacturas.Text = "tabPage1";
            this.tabMailFacturas.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(206, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "MailFacturas";
            // 
            // tabContactos
            // 
            this.tabContactos.Controls.Add(this.btnEliminarContacto);
            this.tabContactos.Controls.Add(this.btnModificarContacto);
            this.tabContactos.Controls.Add(this.pnlTextBoxContactos);
            this.tabContactos.Controls.Add(this.btnAgregarContacto);
            this.tabContactos.Controls.Add(this.btnBuscarContacto);
            this.tabContactos.Controls.Add(this.dgContactos);
            this.tabContactos.Location = new System.Drawing.Point(4, 5);
            this.tabContactos.Name = "tabContactos";
            this.tabContactos.Size = new System.Drawing.Size(402, 656);
            this.tabContactos.TabIndex = 3;
            this.tabContactos.Text = "tabPage2";
            this.tabContactos.UseVisualStyleBackColor = true;
            // 
            // btnEliminarContacto
            // 
            this.btnEliminarContacto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnEliminarContacto.FlatAppearance.BorderSize = 0;
            this.btnEliminarContacto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarContacto.Font = new System.Drawing.Font("Montserrat Medium", 12F);
            this.btnEliminarContacto.ForeColor = System.Drawing.Color.White;
            this.btnEliminarContacto.Location = new System.Drawing.Point(207, 606);
            this.btnEliminarContacto.Margin = new System.Windows.Forms.Padding(0, 15, 0, 10);
            this.btnEliminarContacto.Name = "btnEliminarContacto";
            this.btnEliminarContacto.Size = new System.Drawing.Size(185, 40);
            this.btnEliminarContacto.TabIndex = 17;
            this.btnEliminarContacto.Text = "ELIMINAR";
            this.btnEliminarContacto.UseVisualStyleBackColor = false;
            this.btnEliminarContacto.Click += new System.EventHandler(this.btnEliminarContacto_Click);
            // 
            // btnModificarContacto
            // 
            this.btnModificarContacto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnModificarContacto.FlatAppearance.BorderSize = 0;
            this.btnModificarContacto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificarContacto.Font = new System.Drawing.Font("Montserrat Medium", 12F);
            this.btnModificarContacto.ForeColor = System.Drawing.Color.White;
            this.btnModificarContacto.Location = new System.Drawing.Point(10, 606);
            this.btnModificarContacto.Margin = new System.Windows.Forms.Padding(0, 15, 0, 10);
            this.btnModificarContacto.Name = "btnModificarContacto";
            this.btnModificarContacto.Size = new System.Drawing.Size(185, 40);
            this.btnModificarContacto.TabIndex = 16;
            this.btnModificarContacto.Text = "MODIFICAR";
            this.btnModificarContacto.UseVisualStyleBackColor = false;
            this.btnModificarContacto.Click += new System.EventHandler(this.btnModificarContacto_Click);
            // 
            // pnlTextBoxContactos
            // 
            this.pnlTextBoxContactos.BackColor = System.Drawing.Color.White;
            this.pnlTextBoxContactos.Controls.Add(this.txtBuscarContacto);
            this.pnlTextBoxContactos.Location = new System.Drawing.Point(10, 65);
            this.pnlTextBoxContactos.Margin = new System.Windows.Forms.Padding(15, 0, 15, 15);
            this.pnlTextBoxContactos.Name = "pnlTextBoxContactos";
            this.pnlTextBoxContactos.Size = new System.Drawing.Size(382, 0);
            this.pnlTextBoxContactos.TabIndex = 15;
            // 
            // txtBuscarContacto
            // 
            this.txtBuscarContacto.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBuscarContacto.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscarContacto.Location = new System.Drawing.Point(10, 9);
            this.txtBuscarContacto.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.txtBuscarContacto.Name = "txtBuscarContacto";
            this.txtBuscarContacto.Size = new System.Drawing.Size(362, 26);
            this.txtBuscarContacto.TabIndex = 2;
            // 
            // btnAgregarContacto
            // 
            this.btnAgregarContacto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnAgregarContacto.FlatAppearance.BorderSize = 0;
            this.btnAgregarContacto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarContacto.Font = new System.Drawing.Font("Montserrat Medium", 12F);
            this.btnAgregarContacto.ForeColor = System.Drawing.Color.White;
            this.btnAgregarContacto.Location = new System.Drawing.Point(10, 10);
            this.btnAgregarContacto.Margin = new System.Windows.Forms.Padding(0, 0, 7, 15);
            this.btnAgregarContacto.Name = "btnAgregarContacto";
            this.btnAgregarContacto.Size = new System.Drawing.Size(185, 40);
            this.btnAgregarContacto.TabIndex = 13;
            this.btnAgregarContacto.Text = "AGREGAR";
            this.btnAgregarContacto.UseVisualStyleBackColor = false;
            this.btnAgregarContacto.Click += new System.EventHandler(this.btnAgregarContacto_Click);
            // 
            // btnBuscarContacto
            // 
            this.btnBuscarContacto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnBuscarContacto.FlatAppearance.BorderSize = 0;
            this.btnBuscarContacto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarContacto.Font = new System.Drawing.Font("Montserrat Medium", 12F);
            this.btnBuscarContacto.ForeColor = System.Drawing.Color.White;
            this.btnBuscarContacto.Location = new System.Drawing.Point(207, 10);
            this.btnBuscarContacto.Margin = new System.Windows.Forms.Padding(7, 0, 0, 15);
            this.btnBuscarContacto.Name = "btnBuscarContacto";
            this.btnBuscarContacto.Size = new System.Drawing.Size(185, 40);
            this.btnBuscarContacto.TabIndex = 14;
            this.btnBuscarContacto.Text = "BUSCAR";
            this.btnBuscarContacto.UseVisualStyleBackColor = false;
            // 
            // dgContactos
            // 
            this.dgContactos.AllowUserToAddRows = false;
            this.dgContactos.AllowUserToDeleteRows = false;
            this.dgContactos.AllowUserToResizeColumns = false;
            this.dgContactos.AllowUserToResizeRows = false;
            this.dgContactos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgContactos.BackgroundColor = System.Drawing.Color.White;
            this.dgContactos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgContactos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgContactos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgContactos.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(10, 10, 5, 10);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgContactos.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgContactos.Location = new System.Drawing.Point(10, 65);
            this.dgContactos.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.dgContactos.MultiSelect = false;
            this.dgContactos.Name = "dgContactos";
            this.dgContactos.RowHeadersVisible = false;
            this.dgContactos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgContactos.Size = new System.Drawing.Size(382, 526);
            this.dgContactos.TabIndex = 10;
            this.dgContactos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgContactos_CellDoubleClick);
            // 
            // tabConfiguracion
            // 
            this.tabConfiguracion.Controls.Add(this.groupBox1);
            this.tabConfiguracion.Location = new System.Drawing.Point(4, 5);
            this.tabConfiguracion.Name = "tabConfiguracion";
            this.tabConfiguracion.Size = new System.Drawing.Size(402, 656);
            this.tabConfiguracion.TabIndex = 4;
            this.tabConfiguracion.Text = "tabPage3";
            this.tabConfiguracion.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbExportar);
            this.groupBox1.Controls.Add(this.btnProcesar);
            this.groupBox1.Controls.Add(this.rbImportar);
            this.groupBox1.Controls.Add(this.cbxSeleccionarTabla);
            this.groupBox1.Location = new System.Drawing.Point(11, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(383, 126);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DATOS";
            // 
            // rbExportar
            // 
            this.rbExportar.AutoSize = true;
            this.rbExportar.Location = new System.Drawing.Point(132, 29);
            this.rbExportar.Name = "rbExportar";
            this.rbExportar.Size = new System.Drawing.Size(64, 17);
            this.rbExportar.TabIndex = 16;
            this.rbExportar.Text = "Exportar";
            this.rbExportar.UseVisualStyleBackColor = true;
            this.rbExportar.CheckedChanged += new System.EventHandler(this.rbExportar_CheckedChanged);
            // 
            // btnProcesar
            // 
            this.btnProcesar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(149)))), ((int)(((byte)(168)))));
            this.btnProcesar.FlatAppearance.BorderSize = 0;
            this.btnProcesar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProcesar.Font = new System.Drawing.Font("Montserrat Medium", 12F);
            this.btnProcesar.ForeColor = System.Drawing.Color.White;
            this.btnProcesar.Location = new System.Drawing.Point(243, 58);
            this.btnProcesar.Margin = new System.Windows.Forms.Padding(15, 15, 7, 15);
            this.btnProcesar.Name = "btnProcesar";
            this.btnProcesar.Size = new System.Drawing.Size(130, 40);
            this.btnProcesar.TabIndex = 13;
            this.btnProcesar.Text = "PROCESAR";
            this.btnProcesar.UseVisualStyleBackColor = false;
            this.btnProcesar.Click += new System.EventHandler(this.btnProcesar_Click);
            // 
            // rbImportar
            // 
            this.rbImportar.AutoSize = true;
            this.rbImportar.Checked = true;
            this.rbImportar.Location = new System.Drawing.Point(18, 29);
            this.rbImportar.Name = "rbImportar";
            this.rbImportar.Size = new System.Drawing.Size(63, 17);
            this.rbImportar.TabIndex = 15;
            this.rbImportar.TabStop = true;
            this.rbImportar.Text = "Importar";
            this.rbImportar.UseVisualStyleBackColor = true;
            this.rbImportar.CheckedChanged += new System.EventHandler(this.rbImportar_CheckedChanged);
            // 
            // cbxSeleccionarTabla
            // 
            this.cbxSeleccionarTabla.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxSeleccionarTabla.FormattingEnabled = true;
            this.cbxSeleccionarTabla.Items.AddRange(new object[] {
            "Seleccionar Tabla",
            "Tareas",
            "Contactos"});
            this.cbxSeleccionarTabla.Location = new System.Drawing.Point(18, 71);
            this.cbxSeleccionarTabla.Name = "cbxSeleccionarTabla";
            this.cbxSeleccionarTabla.Size = new System.Drawing.Size(207, 21);
            this.cbxSeleccionarTabla.TabIndex = 11;
            // 
            // PantallaInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(660, 700);
            this.Controls.Add(this.tbcPestanas);
            this.Controls.Add(this.pnlTitleBar);
            this.Controls.Add(this.pnlSideMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "PantallaInicio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestor Tareas Punto Educativo";
            this.Load += new System.EventHandler(this.PantallaInicio_Load);
            this.pnlSideMenu.ResumeLayout(false);
            this.pnlSideHeaderLogo.ResumeLayout(false);
            this.pnlSideHeaderLogo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.pnlTitleBar.ResumeLayout(false);
            this.pnlTitleBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTareasPendientes)).EndInit();
            this.pnlTextBoxTareas.ResumeLayout(false);
            this.pnlTextBoxTareas.PerformLayout();
            this.tbcPestanas.ResumeLayout(false);
            this.tabTareas.ResumeLayout(false);
            this.tabFacturacion.ResumeLayout(false);
            this.tabFacturacion.PerformLayout();
            this.tabMailFacturas.ResumeLayout(false);
            this.tabMailFacturas.PerformLayout();
            this.tabContactos.ResumeLayout(false);
            this.pnlTextBoxContactos.ResumeLayout(false);
            this.pnlTextBoxContactos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgContactos)).EndInit();
            this.tabConfiguracion.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlSideMenu;
        private System.Windows.Forms.Button btnTareas;
        private System.Windows.Forms.Button btnConfiguracion;
        private System.Windows.Forms.Button btnContactos;
        private System.Windows.Forms.Button btnMailFacturas;
        private System.Windows.Forms.Button btnFacturacion;
        private System.Windows.Forms.Panel pnlSideHeaderLogo;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlSelected;
        private System.Windows.Forms.Panel pnlTitleBar;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Button btnMinimizar;
        private System.Windows.Forms.Label lblTitleBar;
        private System.Windows.Forms.Button btnEliminarTareaPendiente;
        private System.Windows.Forms.Button btnVerTareaPendiente;
        private System.Windows.Forms.DataGridView dgTareasPendientes;
        private System.Windows.Forms.Button btnBuscarTarea;
        private System.Windows.Forms.Button btnNuevaTarea;
        private System.Windows.Forms.Timer tmrTextBox;
        private System.Windows.Forms.Panel pnlTextBoxTareas;
        private System.Windows.Forms.TextBox txtInputText;
        private System.Windows.Forms.Timer tmrRecordatorios;
        private System.Windows.Forms.Timer tmrBtnNuevaTarea;
        private System.Windows.Forms.Timer tmrBtnBuscar;
        private System.Windows.Forms.TabControl tbcPestanas;
        private System.Windows.Forms.TabPage tabTareas;
        private System.Windows.Forms.TabPage tabFacturacion;
        private System.Windows.Forms.TabPage tabMailFacturas;
        private System.Windows.Forms.TabPage tabContactos;
        private System.Windows.Forms.TabPage tabConfiguracion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnProcesar;
        private System.Windows.Forms.ComboBox cbxSeleccionarTabla;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbExportar;
        private System.Windows.Forms.RadioButton rbImportar;
        private System.Windows.Forms.DataGridView dgContactos;
        private System.Windows.Forms.Panel pnlTextBoxContactos;
        private System.Windows.Forms.TextBox txtBuscarContacto;
        private System.Windows.Forms.Button btnAgregarContacto;
        private System.Windows.Forms.Button btnBuscarContacto;
        private System.Windows.Forms.Button btnEliminarContacto;
        private System.Windows.Forms.Button btnModificarContacto;
    }
}


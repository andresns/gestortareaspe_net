# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## EN PROCESO

### Agregado
- Se agrega funcionalidad para limpiar base de datos.
- Se agrega opción para Importar/Exportar Datos de Tabla Contactos desde Configuración.

## [1.5.0] - 10-03-2019

### Agregado
- Se habilita pestaña Contactos y se permite agregar, modificar y eliminar contactos.

## [1.4.0] - 19-02-2019

### Agregado
- Se agrega opción para Importar/Exportar Datos de Tabla Tareas desde Configuración.

### Cambios
- Se corrige error que aparece al hacer click en ver tarea cuando no hay ninguna tarea seleccionada.

## [1.3.2] - 09-02-2019

### Cambios
- Se reorganiza codigo fuente.
- Se corrige error en donde al eliminar una tarea con recordatorio, el recordatorio seguía vigente.
- Se limpia la seleccion de la lista de tareas cada vez que se recarga la lista.

## [1.3.1] - 27-01-2019

### Agregado
- Se agrega notificación al usuario cuando no se encuentran resultados en la búsqueda de tareas.

### Cambios
- Se corrige error en donde al guardar tarea sin descripcion se agregaba el placeholder como descripcion.
- Se corrige error en donde al crear tarea nueva no se actualizaban los recordatorios.
- Se mejora efecto hover de Botón Estado Tarea en la pantalla Ver Tarea.

## [1.3.0] - 16-01-2019

### Cambios
- Se actualiza GUI de la Pantalla Ver Tarea.

## [1.2.0] - 14-01-2019

### Agregado
- Se agrega versión en la barra de título de la pantalla principal.

### Cambios
- Se cambia orden de las tareas de la lista de tareas.
- Se cambia imagen en botón cerrar de la barra de título.
- Se actualiza GUI de la Pantalla Nueva Tarea.

## [1.1.2] - 08-01-2019

### Cambios
- Se deshabilitan sonidos de windows al presionar tecla escape o enter en la pestaña 
tareas.
- se corrige error onde al hacer doble click muy rapido en el botón Nueva Tarea se 
invierte la visibilidad del campo de texto.

## [1.1.1] - 07-01-2019

### Cambios
- Se corrige error que impide mover ventana al seleccionar el texto de la barra de título.
- Se corrige funcionamiento de tecla enter en pantalla Nueva Tarea, en donde se guardaba la tarea al presionar enter estando en la descripción.
- Se corrige funcionamiento de la tecla escape en la pestaña Tareas, en donde se mostraba el campo de texto si este se encontraba oculto.
- Se corrige error en donde se mostraba recordatorio de las tareas que estaban terminadas si el cambio de estado de la tarea se producía después de asignar recordatorio.

## [1.1] - 06-01-2019

### Cambios
- Mejora de GUI en pantalla inicio.
- Correción de postergación de recordatorios.

## [1.0] - 29-12-2018

### Agregado
- Primera version estable con gestion de tareas y recordatorios.

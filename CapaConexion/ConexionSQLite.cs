﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace CapaConexion
{
    public class ConexionSQLite
    {
        private string nombreBaseDatos;
        private string nombreTabla;
        private string cadenaConexion;
        private string cadenaSQL;
        private Boolean esSelect;
        private SQLiteConnection dbConnection;
        private DataSet dbDataSet;
        private SQLiteDataAdapter dbDataAdapter;

        public string NombreBaseDatos { get => nombreBaseDatos; set => nombreBaseDatos = value; }
        public string NombreTabla { get => nombreTabla; set => nombreTabla = value; }
        public string CadenaConexion { get => cadenaConexion; set => cadenaConexion = value; }
        public string CadenaSQL { get => cadenaSQL; set => cadenaSQL = value; }
        public bool EsSelect { get => esSelect; set => esSelect = value; }
        public SQLiteConnection DbConnection { get => dbConnection; set => dbConnection = value; }
        public DataSet DbDataSet { get => dbDataSet; set => dbDataSet = value; }
        public SQLiteDataAdapter DbDataAdapter { get => dbDataAdapter; set => dbDataAdapter = value; }

        //Abrir Conexion
        public void abrirConexion()
        {
            try
            {
                this.DbConnection.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al abrir la conexión." + Environment.NewLine + "Error: " + ex.Message, "Sistema");
            }
        } //Fin abrirConexion

        public void cerrarConexion()
        {
            try
            {
                this.DbConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al cerrar la conexión." + Environment.NewLine + "Error: " + ex.Message, "Sistema");
            }
        } //Fin cerrarConexion

        public void conectar()
        {
            if (this.NombreBaseDatos.Length == 0)
            {
                MessageBox.Show("Error en el nombre de la Base de datos.", "Sistema");
                return;
            }

            if (this.NombreTabla.Length == 0)
            {
                MessageBox.Show("Error en el nombre de la tabla.", "Sistema");
                return;
            }

            if (this.CadenaConexion.Length == 0)
            {
                MessageBox.Show("Error en la cadena de conexión.", "Sistema");
                return;
            }

            if (this.CadenaSQL.Length == 0)
            {
                MessageBox.Show("Error en la cadena SQL.", "Sistema");
                return;
            }

            //Se genera instancia de la conexión
            try
            {
                this.DbConnection = new SQLiteConnection(this.CadenaConexion);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error en la conexión." + Environment.NewLine + "Error: " + ex.Message, "Sistema");
                return;
            }

            this.abrirConexion();

            //Instrucciones SQL
            if (this.EsSelect) // SELECT
            {
                this.DbDataSet = new DataSet();
                try
                {
                    this.DbDataAdapter = new SQLiteDataAdapter(this.CadenaSQL, this.DbConnection);
                    this.DbDataAdapter.Fill(this.DbDataSet, this.NombreTabla);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al cargar DataSet." + Environment.NewLine + "Error: " + ex.Message, "Sistema");
                    return;
                }
            }
            else // INSERT - UPDATE - DELETE
            {
                try
                {
                    SQLiteCommand variableSQL = new SQLiteCommand(this.CadenaSQL, this.DbConnection);
                    variableSQL.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error en la instrucción SQL." + Environment.NewLine + "Error: " + ex.Message, "Sistema");
                    return;
                }
            }

            this.cerrarConexion();
        } //Fin Conectar

    } //Fin Clase Conexion
} //Fin namespace
